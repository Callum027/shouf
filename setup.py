#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Shouf package install file.
"""


import setuptools


# Read the package version from file.
with open("VERSION", "r") as f:
    version = f.read().strip()

# Discover the package files.
packages = setuptools.find_packages(
    where=".",
    include=["shouf", "shouf.*"],
)
if not packages:
    raise ValueError("No packages detected")


setuptools.setup(
    name="shouf",
    version=version,
    author="Callum Dickinson",
    author_email="callum.karamu@gmail.com",
    url="https://www.gitlab.com/Callum027/shouf",
    description="Shouf",
    long_description="Shouf",
    packages=packages,
    python_requires=">=3.7",
    install_requires=["arcade", "pyautogui"],
    entry_points={"console_scripts": ["shouf-gui=shouf.gui.window:main"]},
)
