# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

from typing import Optional

from shouf.exceptions import ShoufError


class EngineError(ShoufError):
    """"""

    pass


class LegalityError(ShoufError):
    """"""

    def __init__(self, message: str, player_num: Optional[int] = None):
        """"""
        self.player_num = player_num
        super().__init__(message)


class RuleError(LegalityError):
    """"""

    pass


class ConsistencyError(EngineError):
    """"""

    pass


class WrongPhaseError(LegalityError):
    """"""

    pass
