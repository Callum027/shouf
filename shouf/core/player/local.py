# -*- coding: utf-8 -*-

"""
Player information.
"""


from __future__ import annotations

from shouf.core.player.base import Player


class LocalPlayer(Player):
    """"""

    def copy(self) -> "LocalPlayer":
        """"""
        return LocalPlayer(num=self.num, name=self.name)
