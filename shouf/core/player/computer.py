# -*- coding: utf-8 -*-

"""
Player information.
"""


from __future__ import annotations

from typing import TYPE_CHECKING

from shouf.core.player.base import Player

if TYPE_CHECKING:
    from shouf.computer import Difficulty


class ComputerPlayer(Player):
    """"""

    def __init__(self, num: int, difficulty: Difficulty):
        """"""
        self.difficulty = difficulty
        super().__init__(num=num, name=f"Computer {num}")

    def copy(self) -> "ComputerPlayer":
        """"""
        return ComputerPlayer(num=self.num, difficulty=self.difficulty)
