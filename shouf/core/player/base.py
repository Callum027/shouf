# -*- coding: utf-8 -*-

"""
Player information.
"""


from __future__ import annotations


class Player:
    """"""

    def __init__(self, num: int, name: str):
        """"""
        self.num = num
        self.name = name

    def copy(self) -> "Player":
        """"""
        raise NotImplementedError()
