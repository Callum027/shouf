# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

import enum

from typing import Dict, Iterable, List, Optional, Set, Tuple

from shouf.core.card import Card
from shouf.core.card_pile import CardPile


class Phase(enum.IntEnum):
    """"""

    NEW_STATE = 0
    NEW_GAME = 1
    DECK_SHUFFLED = 2
    CARDS_DEALED = 3
    SHOWN_FRONT_TWO_CARDS = 4
    NEW_TURN = 5
    CARD_DRAWN = 6
    CARD_USED = 7
    CARD_EFFECT_USED = 8
    CARD_STACKED = 9
    SHOUF_CALLED = 10
    END_GAME = 11


class State:
    def __init__(
        self,
        player_nums: Optional[Iterable[int]] = None,
        previous_state: Optional["State"] = None,
    ):
        """"""

        # If not deep copying antoher State object, initialise a brand new State object.
        if not previous_state:
            #
            self.player_nums = sorted(player_nums)
            #
            self.current_game = 0
            self.previous_scores: Dict[int, List[int]] = {
                player_num: [] for player_num in self.player_nums
            }
            #
            self.deck = CardPile(with_jokers=False)
            self.discard_pile = CardPile(start_empty=True)
            self.hand_card: Optional[Card] = None
            self.player_piles: Dict[int, Dict[int, Card]] = {
                player_num: {} for player_num in self.player_nums
            }
            #
            self.current_turn = 0
            self.current_phase = Phase.NEW_STATE
            #
            self.current_player = 0
            self.shouf_player: Optional[int] = None
            #
            self.drew_from_deck = False
            self.drew_from_discard = False
            #
            self.stacked_player: Optional[int] = None
            self.stacked_previous_targets: List[Tuple[int, int]] = []
            #
            self.ended_turn_players: Set[int] = set()
        # If previous_state was passed, initialise this State object as a deep copy of that state.
        else:
            #
            self.player_nums = list(previous_state.player_nums)
            #
            self.current_game = previous_state.current_game
            self.previous_scores = {
                player_num: list(scores)
                for player_num, scores in previous_state.previous_scores.items()
            }
            #
            self.deck = previous_state.deck.copy()
            self.discard_pile = previous_state.discard_pile.copy()
            self.hand_card = previous_state.hand_card.copy() if previous_state.hand_card else None
            self.player_piles = {
                player_num: {card_num: card.copy() for card_num, card in pile.items()}
                for player_num, pile in previous_state.player_piles.items()
            }
            #
            self.current_turn = previous_state.current_turn
            self.current_phase = previous_state.current_phase
            #
            self.current_player = previous_state.current_player
            self.shouf_player = previous_state.shouf_player
            #
            self.drew_from_deck = previous_state.drew_from_deck
            self.drew_from_discard = previous_state.drew_from_discard
            #
            self.stacked_player = previous_state.stacked_player
            self.stacked_previous_targets = [
                target for target in previous_state.stacked_previous_targets
            ]
            #
            self.ended_turn_players = previous_state.ended_turn_players.copy()

    @property
    def started(self) -> bool:
        """"""
        return self.current_phase >= Phase.NEW_TURN

    @property
    def ended(self) -> bool:
        """"""
        return self.current_phase == Phase.END_GAME

    @property
    def num_players(self) -> int:
        """"""
        return len(self.player_piles)

    @property
    def player_scores(self) -> Dict[int, int]:
        """"""
        #
        scores = {
            player_num: sum(card.points for card in pile.values())
            for player_num, pile in self.player_piles.items()
        }
        #
        if self.shouf_player:
            for player_num, score in scores.items():
                if player_num != self.shouf_player and scores[self.shouf_player] > score:
                    scores[self.shouf_player] += 25
                    break
        #
        return scores

    @property
    def winning_players(self) -> List[int]:
        """"""
        player_nums: List[int] = []
        score = 0
        for pn, s in self.player_scores.items():
            if not player_nums or s < score:
                player_nums = [pn]
                score = s
            elif s == score:
                player_nums.append(pn)
        return player_nums

    @property
    def losing_players(self) -> List[int]:
        """"""
        player_nums: List[int] = []
        score = 0
        for pn, s in self.player_scores.items():
            if not player_nums or s > score:
                player_nums = [pn]
                score = s
            elif s == score:
                player_nums.append(pn)
        return player_nums

    def copy(self) -> "State":
        """"""
        return State(previous_state=self)

    def __str__(self) -> str:
        """"""
        #
        previous_scores = (
            "{\n"
            + "\n".join(
                (
                    ("        " + str(pn) + ": " + str(ss) + ",")
                    for pn, ss in self.previous_scores.items()
                ),
            )
            + "\n    }"
        )
        #
        deck = str(self.deck)
        deck_lines = deck.splitlines()
        if len(deck_lines) > 1:
            for i, line in enumerate(deck_lines[1:-1], 1):
                deck_lines[i] = f"    {line}"
            deck_lines[-1] = f"    {deck_lines[-1]}"
            deck = "\n".join(deck_lines)
        #
        discard_pile = str(self.discard_pile)
        discard_card_lines = discard_pile.splitlines()
        if len(discard_card_lines) > 1:
            for i, line in enumerate(discard_card_lines[1:-1], 1):
                discard_card_lines[i] = f"    {line}"
            discard_card_lines[-1] = f"    {discard_card_lines[-1]}"
            discard_pile = "\n".join(discard_card_lines)
        #
        player_piles = "{\n"
        for player_num, pile in self.player_piles.items():
            p = f"        {player_num}: " + "{"
            if pile:
                p += (
                    "\n"
                    + "\n".join(
                        (f"            {card_num}: {card}," for card_num, card in pile.items()),
                    )
                    + "\n        "
                )
            p += "},\n"
            player_piles += p
        player_piles += "    }"
        #
        return (
            "{\n"
            f"    'player_nums': {self.player_nums},\n"
            f"    'current_game': {self.current_game},\n"
            f"    'previous_scores': {previous_scores},\n"
            f"    'current_game': {self.current_game},\n"
            f"    'deck': {deck},\n"
            f"    'discard_pile': {discard_pile},\n"
            f"    'hand_card': {self.hand_card},\n"
            f"    'player_piles': {player_piles},\n"
            f"    'current_turn': {self.current_turn},\n"
            f"    'current_phase': Phase.{self.current_phase.name},\n"
            f"    'current_player': {self.current_player},\n"
            f"    'shouf_player': {self.shouf_player},\n"
            f"    'drew_from_deck': {self.drew_from_deck},\n"
            f"    'drew_from_discard': {self.drew_from_discard},\n"
            f"    'stacked_player': {self.stacked_player},\n"
            f"    'stacked_previous_targets': {self.stacked_previous_targets},\n"
            f"    'ended_turn_players': set({sorted(self.ended_turn_players)}),\n"
            "}"
        )
