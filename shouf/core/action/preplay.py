# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

from shouf.core.state import State, Phase

from shouf.core.action.base import PlayerAction
from shouf.core.action.listener.base import ActionListener

from shouf.core.exceptions import ConsistencyError, LegalityError, WrongPhaseError


class PreplayAction(PlayerAction):
    """"""

    pass


class CallShoufAction(PreplayAction):
    """"""

    def check_legality(self, state: State) -> None:
        """"""
        if self.player_num != state.current_player:
            raise LegalityError(
                "You cannot call Shouf when it is not your turn",
                player_num=self.player_num,
            )
        if state.shouf_player:
            raise LegalityError(
                f"Shouf has already been called by player {state.shouf_player}",
                player_num=self.player_num,
            )
        if state.current_phase != Phase.NEW_TURN:
            raise WrongPhaseError(
                "Shouf can only be called at the start of a turn",
                player_num=self.player_num,
            )

    def apply(self, state: State) -> State:
        """"""
        new_state = state.copy()
        new_state.shouf_player = self.player_num
        new_state.current_phase = Phase.SHOUF_CALLED
        return new_state

    def notify(self, listener: ActionListener) -> None:
        """"""
        listener.notify_call_shouf(player_num=self.player_num)

    def __str__(self) -> str:
        """"""
        return f"CallShoufAction(player_num={self.player_num})"


class DrawDeckAction(PreplayAction):
    """"""

    def check_legality(self, state: State) -> None:
        """"""
        if self.player_num != state.current_player:
            raise LegalityError(
                "You cannot draw from the deck when it is not your turn",
                player_num=self.player_num,
            )
        if self.player_num == state.shouf_player:
            raise LegalityError(
                "You've called Shouf, you can't make any moves",
                player_num=self.player_num,
            )
        if state.current_phase >= Phase.CARD_DRAWN:
            raise WrongPhaseError(
                "You have already drawn a card this turn",
                player_num=self.player_num,
            )

    def apply(self, state: State) -> State:
        """"""
        new_state = state.copy()
        if new_state.hand_card:
            raise ConsistencyError("There is already a card in the player's hand")
        new_state.hand_card = new_state.deck.draw()
        new_state.current_phase = Phase.CARD_DRAWN
        new_state.drew_from_deck = True
        return new_state

    def notify(self, listener: ActionListener) -> None:
        """"""
        listener.notify_draw_from_deck(player_num=self.player_num)

    def __str__(self) -> str:
        """"""
        return f"DrawDeckAction(player_num={self.player_num})"


class DrawDiscardAction(PreplayAction):
    """"""

    def check_legality(self, state: State) -> None:
        """"""
        if self.player_num != state.current_player:
            raise LegalityError(
                "You cannot draw from the discard pile when it is not your turn",
                player_num=self.player_num,
            )
        if self.player_num == state.shouf_player:
            raise LegalityError(
                "You've called Shouf, you can't make any moves",
                player_num=self.player_num,
            )
        if state.current_phase >= Phase.CARD_DRAWN:
            raise WrongPhaseError(
                "You have already drawn a card this turn",
                player_num=self.player_num,
            )

    def apply(self, state: State) -> State:
        """"""
        new_state = state.copy()
        if new_state.hand_card:
            raise ConsistencyError("There is already a card in the player's hand")
        new_state.hand_card = new_state.discard_pile.draw()
        new_state.current_phase = Phase.CARD_DRAWN
        new_state.drew_from_discard = True
        return new_state

    def notify(self, listener: ActionListener) -> None:
        """"""
        listener.notify_draw_from_discard(player_num=self.player_num)

    def __str__(self) -> str:
        """"""
        return f"DrawDiscardAction(player_num={self.player_num})"
