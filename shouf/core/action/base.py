# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

from shouf.core.state import State
from shouf.core.action.listener.base import ActionListener


class Action:
    """"""

    def check_legality(self, state: State) -> None:
        """"""
        raise NotImplementedError()

    def apply(self, state: State) -> State:
        """"""
        raise NotImplementedError()

    def notify(self, listener: ActionListener) -> None:
        """"""
        raise NotImplementedError()

    def __str__(self) -> str:
        """"""
        raise NotImplementedError()


class PlayerAction(Action):
    """"""

    def __init__(self, player_num: int):
        """"""
        self.player_num = player_num
