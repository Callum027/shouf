# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

import logging

from typing import Dict, Iterable, Optional, Sequence

from shouf.core.card import Card
from shouf.core.card_pile import CardPile
from shouf.core.state import State, Phase
from shouf.core.player.base import Player

from shouf.core.action.listener.base import ActionListener


class ActionLogger(ActionListener):
    """"""

    def __init__(
        self,
        log_level: str = "INFO",
        to_stdout: bool = True,
        handlers: Optional[Iterable[logging.Handler]] = None,
    ):
        """"""
        super().__init__()
        #
        self.current_turn = 0
        #
        self.logger = logging.getLogger("shouf")
        self.logger.setLevel(log_level)
        self.logger.handlers = []
        formatter = logging.Formatter("%(message)s")
        if to_stdout:
            stream_handler = logging.StreamHandler()
            stream_handler.setFormatter(formatter)
            self.logger.addHandler(stream_handler)
        if handlers:
            for handler in handlers:
                self.logger.addHandler(handler)
        self.logger.debug("Action logger started.")

    @property
    def players(self) -> Dict[int, Player]:
        """"""
        return self.engine.players

    @property
    def current_state(self) -> State:
        """"""
        return self.engine.current_state

    @property
    def current_phase(self) -> Phase:
        """"""
        return self.current_state.current_phase

    @property
    def discard_pile(self) -> CardPile:
        """"""
        return self.current_state.discard_pile

    def on_new_game(self) -> None:
        """"""
        self.logger.info(
            "A new game will begin! "
            f"The starting player is {self.players[self.current_state.current_player].name}.",
        )

    def on_shuffle_deck(self, shuffled_cards: Sequence[Card]) -> None:
        """"""
        self.logger.info("The deck has been shuffled.")

    def on_deal_card(self, player_num: int, card_num: int) -> None:
        """"""
        pass

    def on_finish_dealing_cards(self) -> None:
        """"""
        self.logger.info("Cards have been dealed to all players.")

    def on_show_front_two_cards(self) -> None:
        """"""
        self.logger.info("Showing all players their front two cards.")

    def on_start_game(self) -> None:
        """"""
        self.current_turn = self.current_state.current_turn
        self.logger.info("The game has started!")
        # self.logger.info(f"Current state: {self.current_state}")

    def on_call_shouf(self, player_num: int) -> None:
        """"""
        self.logger.info(
            f"{self.players[player_num].name} has called Shouf! "
            "The game will finish after everyone has had their last turn.",
        )

    def on_draw_from_deck(self, player_num: int) -> None:
        """"""
        self.logger.info(f"{self.players[player_num].name} draws from the deck!")

    def on_draw_from_discard(self, player_num: int) -> None:
        """"""
        self.logger.info(f"{self.players[player_num].name} draws from the discard pile!")

    def on_switch_cards(self, player_num: int, card_num: int) -> None:
        """"""
        self.logger.info(
            f"{self.players[player_num].name} switches their "
            f"{self.discard_pile.get_top_card().description} with the card they drew!",
        )

    def on_play_card(self, player_num: int) -> None:
        """"""
        card_description = self.discard_pile.get_top_card().description
        an = "an" if card_description[0].lower() in ("a", "e", "i", "o", "u") else "a"
        self.logger.info(f"{self.players[player_num].name} plays {an} {card_description}!")

    def on_swap_players_cards(
        self,
        player_num: int,
        card_num: int,
        other_player_num: int,
        other_card_num: int,
        show_cards: bool,
    ) -> None:
        """"""
        self.logger.info(
            f"{self.players[player_num].name} swaps their card {card_num} with "
            f"{self.players[other_player_num].name}'s card {other_card_num}!",
        )

    def on_show_card(self, player_num: int, target_player_num: int, target_card_num: int) -> None:
        """"""
        if player_num == target_player_num:
            self.logger.info(
                f"{self.players[player_num].name} takes a peek at "
                f"their own card {target_card_num}!",
            )
        else:
            self.logger.info(
                f"{self.players[player_num].name} takes a peek at "
                f"{self.players[target_player_num].name}'s card {target_card_num}!",
            )

    def on_no_card_effect(self, player_num: int) -> None:
        """"""
        self.logger.info(
            f"{self.players[player_num].name} has decided not to use the card effect this time.",
        )

    def on_stack_card(self, player_num: int, target_player_num: int, target_card_num: int) -> None:
        """"""
        if player_num == target_player_num:
            self.logger.info(
                f"{self.players[player_num].name} stacks their own card {target_card_num}!",
            )
        else:
            self.logger.info(
                f"{self.players[player_num].name} stacks "
                f"{self.players[target_player_num].name}'s card {target_card_num}!",
            )

    def on_move_card(
        self,
        player_num: int,
        card_num: int,
        target_player_num: int,
        target_card_num: int,
    ) -> None:
        """"""
        self.logger.info(
            f"{self.players[player_num].name} replaces "
            f"{self.players[target_player_num].name}'s card {target_card_num} "
            f"with their own card {card_num}!",
        )

    def on_end_turn(self, player_num: int) -> None:
        """"""
        self.logger.info(f"{self.players[player_num].name} has finished play for this turn.")
        #
        if self.current_turn < self.current_state.current_turn:
            self.current_turn = self.current_state.current_turn
            self.logger.info(f"Turn {self.current_turn} has ended.")
            # self.logger.info(f"Current state: {self.current_state}")
        #
        if self.current_phase == Phase.END_GAME:
            #
            winning_players = self.current_state.winning_players
            if not winning_players:
                raise ValueError(f"Invalid state for winning_players: {winning_players}")
            #
            self.logger.info("The game has ended.")
            self.logger.info("Results:")
            #
            for player_num, score in self.current_state.player_scores.items():
                #
                card_points = [
                    card.points for card in self.current_state.player_piles[player_num].values()
                ]
                #
                message = f"  * {self.players[player_num].name}: "
                #
                if card_points:
                    message += str(card_points[0])
                    if len(card_points) > 1:
                        for cp in card_points[1:]:
                            message += f" {'-' if cp < 0 else '+'} {abs(cp)}"
                else:
                    message += "No Cards (0)"
                #
                if (
                    player_num == self.current_state.shouf_player
                    and player_num not in winning_players
                ):
                    message += " + Shouf Penalty (25)"
                #
                message += f" = {score} point{'s' if score != 1 else ''}"
                self.logger.info(message)
            #
            if len(winning_players) == 1:
                self.logger.info(f"{self.players[winning_players[0]].name} wins!")
            else:
                self.logger.info(
                    ", ".join(self.players[pn].name for pn in winning_players[:-1])
                    + f" and {self.players[winning_players[-1]].name} win!"
                )
