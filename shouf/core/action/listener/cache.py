# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

from queue import Queue, Empty as QueueEmpty

from typing import Any, List, Sequence, Tuple

from shouf.core.card import Card

from shouf.core.action.listener.base import ActionListener


class ActionListenerCache(ActionListener):
    """"""

    def __init__(self):
        """"""
        self.actions: Queue[Tuple[str, List[Any]]] = Queue(0)
        super().__init__()

    def notify_actions(self) -> None:
        """"""
        if not self.actions:
            return
        try:
            while True:
                action_name, args = self.actions.get_nowait()
                getattr(self, f"on_{action_name}")(*args)
        except QueueEmpty:
            return

    #
    #
    #

    def notify_new_game(self) -> None:
        """"""
        self.actions.put(("new_game", []))

    def notify_shuffle_deck(self, shuffled_cards: Sequence[Card]) -> None:
        """"""
        self.actions.put(("shuffle_deck", [shuffled_cards]))

    def notify_deal_card(self, player_num: int, card_num: int) -> None:
        """"""
        self.actions.put(("deal_card", [player_num, card_num]))

    def notify_finish_dealing_cards(self) -> None:
        """"""
        self.actions.put(("finish_dealing_cards", []))

    def notify_show_front_two_cards(self) -> None:
        """"""
        self.actions.put(("show_front_two_cards", []))

    def notify_start_game(self) -> None:
        """"""
        self.actions.put(("start_game", []))

    def notify_call_shouf(self, player_num: int) -> None:
        """"""
        self.actions.put(("call_shouf", [player_num]))

    def notify_draw_from_deck(self, player_num: int) -> None:
        """"""
        self.actions.put(("draw_from_deck", [player_num]))

    def notify_draw_from_discard(self, player_num: int) -> None:
        """"""
        self.actions.put(("draw_from_discard", [player_num]))

    def notify_switch_cards(self, player_num: int, card_num: int) -> None:
        """"""
        self.actions.put(("switch_cards", [player_num, card_num]))

    def notify_play_card(self, player_num: int) -> None:
        """"""
        self.actions.put(("play_card", [player_num]))

    def notify_swap_players_cards(
        self,
        player_num: int,
        card_num: int,
        other_player_num: int,
        other_card_num: int,
        show_cards: bool,
    ) -> None:
        """"""
        self.actions.put(
            (
                "swap_players_cards",
                [player_num, card_num, other_player_num, other_card_num, show_cards],
            ),
        )

    def notify_show_card(
        self, player_num: int, target_player_num: int, target_card_num: int
    ) -> None:
        """"""
        self.actions.put(("show_card", [player_num, target_player_num, target_card_num]))

    def notify_no_card_effect(self, player_num: int) -> None:
        """"""
        self.actions.put(("no_card_effect", [player_num]))

    def notify_stack_card(
        self, player_num: int, target_player_num: int, target_card_num: int
    ) -> None:
        """"""
        self.actions.put(("stack_card", [player_num, target_player_num, target_card_num]))

    def notify_move_card(
        self,
        player_num: int,
        card_num: int,
        target_player_num: int,
        target_card_num: int,
    ) -> None:
        """"""
        self.actions.put(
            ("move_card", [player_num, card_num, target_player_num, target_card_num]),
        )

    def notify_end_turn(self, player_num: int) -> None:
        """"""
        self.actions.put(("end_turn", [player_num]))
