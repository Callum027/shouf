# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

from typing import TYPE_CHECKING, Optional, Sequence

from shouf.core.card import Card

if TYPE_CHECKING:
    from shouf.core.engine import Engine


class ActionListener:
    """"""

    def __init__(self) -> None:
        """"""
        self.engine: Optional[Engine] = None

    def subscribe_to_engine(self, engine: Engine) -> None:
        """"""
        self.engine = engine

    #
    #
    #

    def notify_new_game(self) -> None:
        """"""
        self.on_new_game()

    def notify_shuffle_deck(self, shuffled_cards: Sequence[Card]) -> None:
        """"""
        self.on_shuffle_deck(shuffled_cards)

    def notify_deal_card(self, player_num: int, card_num: int) -> None:
        """"""
        self.on_deal_card(player_num, card_num)

    def notify_finish_dealing_cards(self) -> None:
        """"""
        self.on_finish_dealing_cards()

    def notify_show_front_two_cards(self) -> None:
        """"""
        self.on_show_front_two_cards()

    def notify_start_game(self) -> None:
        """"""
        self.on_start_game()

    def notify_call_shouf(self, player_num: int) -> None:
        """"""
        self.on_call_shouf(player_num)

    def notify_draw_from_deck(self, player_num: int) -> None:
        """"""
        self.on_draw_from_deck(player_num)

    def notify_draw_from_discard(self, player_num: int) -> None:
        """"""
        self.on_draw_from_discard(player_num)

    def notify_switch_cards(self, player_num: int, card_num: int) -> None:
        """"""
        self.on_switch_cards(player_num, card_num)

    def notify_play_card(self, player_num: int) -> None:
        """"""
        self.on_play_card(player_num)

    def notify_swap_players_cards(
        self,
        player_num: int,
        card_num: int,
        other_player_num: int,
        other_card_num: int,
        show_cards: bool,
    ) -> None:
        """"""
        self.on_swap_players_cards(
            player_num,
            card_num,
            other_player_num,
            other_card_num,
            show_cards,
        )

    def notify_show_card(
        self,
        player_num: int,
        target_player_num: int,
        target_card_num: int,
    ) -> None:
        """"""
        self.on_show_card(player_num, target_player_num, target_card_num)

    def notify_no_card_effect(self, player_num: int) -> None:
        """"""
        self.on_no_card_effect(player_num)

    def notify_stack_card(
        self,
        player_num: int,
        target_player_num: int,
        target_card_num: int,
    ) -> None:
        """"""
        self.on_stack_card(player_num, target_player_num, target_card_num)

    def notify_move_card(
        self,
        player_num: int,
        card_num: int,
        target_player_num: int,
        target_card_num: int,
    ) -> None:
        """"""
        self.on_move_card(player_num, card_num, target_player_num, target_card_num)

    def notify_end_turn(self, player_num: int) -> None:
        """"""
        self.on_end_turn(player_num)

    #
    #
    #

    def on_new_game(self) -> None:
        """"""
        raise NotImplementedError()

    def on_shuffle_deck(self, shuffled_cards: Sequence[Card]) -> None:
        """"""
        raise NotImplementedError()

    def on_deal_card(self, player_num: int, card_num: int) -> None:
        """"""
        raise NotImplementedError()

    def on_finish_dealing_cards(self) -> None:
        """"""
        raise NotImplementedError()

    def on_show_front_two_cards(self) -> None:
        """"""
        raise NotImplementedError()

    def on_start_game(self) -> None:
        """"""
        raise NotImplementedError()

    def on_call_shouf(self, player_num: int) -> None:
        """"""
        raise NotImplementedError()

    def on_draw_from_deck(self, player_num: int) -> None:
        """"""
        raise NotImplementedError()

    def on_draw_from_discard(self, player_num: int) -> None:
        """"""
        raise NotImplementedError()

    def on_switch_cards(self, player_num: int, card_num: int) -> None:
        """"""
        raise NotImplementedError()

    def on_play_card(self, player_num: int) -> None:
        """"""
        raise NotImplementedError()

    def on_swap_players_cards(
        self,
        player_num: int,
        card_num: int,
        other_player_num: int,
        other_card_num: int,
        show_cards: bool,
    ) -> None:
        """"""
        raise NotImplementedError()

    def on_show_card(self, player_num: int, target_player_num: int, target_card_num: int) -> None:
        """"""
        raise NotImplementedError()

    def on_no_card_effect(self, player_num: int) -> None:
        """"""
        raise NotImplementedError()

    def on_stack_card(self, player_num: int, target_player_num: int, target_card_num: int) -> None:
        """"""
        raise NotImplementedError()

    def on_move_card(
        self,
        player_num: int,
        card_num: int,
        target_player_num: int,
        target_card_num: int,
    ) -> None:
        """"""
        raise NotImplementedError()

    def on_end_turn(self, player_num: int) -> None:
        """"""
        raise NotImplementedError()
