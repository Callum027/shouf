# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

from shouf.core.card import Rank

from shouf.core.state import State, Phase

from shouf.core.action.base import PlayerAction
from shouf.core.action.listener.base import ActionListener

from shouf.core.exceptions import LegalityError, WrongPhaseError


class CardEffectAction(PlayerAction):
    """"""

    pass


class SwapPlayersCardsAction(CardEffectAction):
    """"""

    def __init__(
        self,
        player_num: int,
        card_num: int,
        other_player_num: int,
        other_card_num: int,
        show_cards: bool,
    ):
        """"""
        super().__init__(player_num)
        self.card_num = card_num
        self.other_player_num = other_player_num
        self.other_card_num = other_card_num
        self.show_cards = show_cards

    def check_legality(self, state: State) -> None:
        """"""
        #
        if self.player_num != state.current_player:
            raise LegalityError(
                "You cannot swap players' cards when it is not your turn",
                player_num=self.player_num,
            )
        #
        if self.player_num == state.shouf_player:
            raise LegalityError(
                "You've called Shouf, you can't make any moves",
                player_num=self.player_num,
            )
        #
        if self.other_player_num == state.shouf_player:
            raise LegalityError("That player's cards are Shouf locked", player_num=self.player_num)
        #
        if state.current_phase >= Phase.CARD_EFFECT_USED:
            raise WrongPhaseError(
                "You have already used a card effect this turn",
                player_num=self.player_num,
            )
        #
        if state.current_phase != Phase.CARD_USED:
            raise WrongPhaseError(
                f"Incorrect phase for this action: {state.current_phase}",
                player_num=self.player_num,
            )
        #
        card = state.discard_pile.get_top_card()
        if card.rank in (Rank.JACK, Rank.QUEEN):
            if self.show_cards:
                raise LegalityError(
                    "You may not look at the cards being swapped if you played a Jack or Queen",
                    player_num=self.player_num,
                )
        elif card.suit.is_black() and card.rank == Rank.KING:
            pass
        else:
            raise LegalityError(
                "This card effect cannot be used with the played card",
                player_num=self.player_num,
            )

    def apply(self, state: State) -> State:
        """"""
        #
        new_state = state.copy()
        #
        player_pile = new_state.player_piles[self.player_num]
        other_player_pile = new_state.player_piles[self.other_player_num]
        #
        player_card = player_pile[self.card_num]
        other_player_card = other_player_pile[self.other_card_num]
        #
        player_pile[self.card_num] = other_player_card
        other_player_pile[self.other_card_num] = player_card
        #
        new_state.current_phase = Phase.CARD_EFFECT_USED
        #
        return new_state

    def notify(self, listener: ActionListener) -> None:
        """"""
        listener.notify_swap_players_cards(
            self.player_num,
            self.card_num,
            self.other_player_num,
            self.other_card_num,
            self.show_cards,
        )

    def __str__(self) -> str:
        """"""
        return (
            "SwapPlayersCardsAction("
            f"player_num={self.player_num}, "
            f"card_num={self.card_num}, "
            f"other_player_num={self.other_player_num}, "
            f"other_card_num={self.other_card_num}, "
            f"show_cards={self.show_cards}"
            ")"
        )


class ShowCardAction(CardEffectAction):
    """"""

    def __init__(
        self,
        player_num: int,
        target_player_num: int,
        target_card_num: int,
    ):
        """"""
        super().__init__(player_num)
        self.target_player_num = target_player_num
        self.target_card_num = target_card_num

    def check_legality(self, state: State) -> None:
        """"""
        #
        if self.player_num != state.current_player:
            raise LegalityError(
                "You cannot look at a card when it is not your turn",
                player_num=self.player_num,
            )
        #
        if self.player_num == state.shouf_player:
            raise LegalityError(
                "You've called Shouf, you can't make any moves",
                player_num=self.player_num,
            )
        #
        if self.target_player_num == state.shouf_player:
            raise LegalityError("That player's cards are Shouf locked", player_num=self.player_num)
        #
        if state.current_phase >= Phase.CARD_EFFECT_USED:
            raise WrongPhaseError(
                "You have already used a card effect this turn",
                player_num=self.player_num,
            )
        #
        if state.current_phase != Phase.CARD_USED:
            raise WrongPhaseError(
                f"Incorrect phase for this action: {state.current_phase}",
                player_num=self.player_num,
            )
        #
        card = state.discard_pile.get_top_card()
        if card.rank in (Rank.SEVEN, Rank.EIGHT):
            if self.player_num != self.target_player_num:
                raise LegalityError(
                    "You may only look at one of your own cards if you played a 7 or 8",
                    player_num=self.player_num,
                )
        elif card.rank in (Rank.NINE, Rank.TEN):
            if self.player_num == self.target_player_num:
                raise LegalityError(
                    "You may only look at another player's card if you played a 9 or 10",
                    player_num=self.player_num,
                )
        else:
            raise LegalityError(
                "This card effect cannot be used with the played card",
                player_num=self.player_num,
            )

    def apply(self, state: State) -> State:
        """"""
        new_state = state.copy()
        new_state.current_phase = Phase.CARD_EFFECT_USED
        return new_state

    def notify(self, listener: ActionListener) -> None:
        """"""
        listener.notify_show_card(
            player_num=self.player_num,
            target_player_num=self.target_player_num,
            target_card_num=self.target_card_num,
        )

    def __str__(self) -> str:
        """"""
        return (
            "ShowCardAction("
            f"player_num={self.player_num}, "
            f"target_player_num={self.target_player_num}, "
            f"target_card_num={self.target_card_num}"
            ")"
        )


class NoCardEffectAction(CardEffectAction):
    """"""

    def check_legality(self, state: State) -> None:
        """"""
        #
        if self.player_num != state.current_player:
            raise LegalityError(
                "You cannot specify no card effect when it is not your turn",
                player_num=self.player_num,
            )
        #
        if self.player_num == state.shouf_player:
            raise LegalityError(
                "You've called Shouf, you can't make any moves",
                player_num=self.player_num,
            )
        #
        if state.current_phase >= Phase.CARD_EFFECT_USED:
            raise WrongPhaseError(
                "You have already used a card effect this turn",
                player_num=self.player_num,
            )
        #
        if state.current_phase != Phase.CARD_USED:
            raise WrongPhaseError(
                f"Incorrect phase for this action: {state.current_phase}",
                player_num=self.player_num,
            )

    def apply(self, state: State) -> State:
        """"""
        new_state = state.copy()
        new_state.current_phase = Phase.CARD_EFFECT_USED
        return new_state

    def notify(self, listener: ActionListener) -> None:
        """"""
        listener.notify_no_card_effect(player_num=self.player_num)

    def __str__(self) -> str:
        """"""
        return f"NoCardEffectAction(player_num={self.player_num})"
