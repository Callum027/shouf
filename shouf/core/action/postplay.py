# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

from shouf.core.state import State, Phase

from shouf.core.action.base import PlayerAction
from shouf.core.action.listener.base import ActionListener

from shouf.core.exceptions import RuleError, ConsistencyError, LegalityError, WrongPhaseError


class PostplayAction(PlayerAction):
    """"""

    pass


class StackCardAction(PostplayAction):
    """"""

    def __init__(self, player_num: int, target_player_num: int, target_card_num: int):
        """"""
        super().__init__(player_num)
        self.target_player_num = target_player_num
        self.target_card_num = target_card_num

    def check_legality(self, state: State) -> None:
        """"""
        if state.current_phase not in (Phase.CARD_EFFECT_USED, Phase.CARD_STACKED):
            raise WrongPhaseError(
                "You cannot stack cards until after a card has been played",
                player_num=self.player_num,
            )
        if self.player_num in state.ended_turn_players:
            raise LegalityError(
                "You have already finished play for this turn",
                player_num=self.player_num,
            )
        if self.player_num == state.shouf_player:
            raise LegalityError(
                "You've called Shouf, you can't make any moves",
                player_num=self.player_num,
            )
        if self.target_player_num == state.shouf_player:
            raise LegalityError("That player's cards are Shouf locked", player_num=self.player_num)
        if state.stacked_player is not None and state.stacked_player != self.player_num:
            raise LegalityError(
                "Only the player who stacked cards first may stack additional cards",
                player_num=self.player_num,
            )
        if self.target_card_num not in state.player_piles[self.target_player_num]:
            raise LegalityError(
                f"The target player does not have a card in slot {self.target_card_num}",
                player_num=self.player_num,
            )
        if not state.discard_pile:
            raise LegalityError(
                "There are no cards in the discard pile",
                player_num=self.player_num,
            )
        target_card = state.player_piles[self.target_player_num][self.target_card_num]
        if target_card.rank != state.discard_pile.get_top_card().rank:
            raise LegalityError(
                "The stacked card is not the same rank as the top card on the discard pile",
                player_num=self.player_num,
            )

    def apply(self, state: State) -> State:
        """"""
        new_state = state.copy()
        #
        if new_state.stacked_player and self.player_num != new_state.stacked_player:
            raise RuleError(f"Player {self.player_num} cannot stack cards this turn")
        #
        target_player_pile = new_state.player_piles[self.target_player_num]
        target_card = target_player_pile[self.target_card_num]
        #
        new_state.discard_pile.place(target_card)
        del target_player_pile[self.target_card_num]
        #
        if not new_state.stacked_player:
            new_state.stacked_player = self.player_num
        #
        if self.target_player_num != self.player_num:
            new_state.stacked_previous_targets.append(
                (self.target_player_num, self.target_card_num),
            )
        #
        new_state.current_phase = Phase.CARD_STACKED
        #
        return new_state

    def notify(self, listener: ActionListener) -> None:
        """"""
        listener.notify_stack_card(
            player_num=self.player_num,
            target_player_num=self.target_player_num,
            target_card_num=self.target_card_num,
        )

    def __str__(self) -> str:
        """"""
        return (
            "StackCardAction("
            f"player_num={self.player_num}, "
            f"target_player_num={self.target_player_num}, "
            f"target_card_num={self.target_card_num}"
            ")"
        )


class MoveCardAction(PostplayAction):
    """"""

    def __init__(
        self,
        player_num: int,
        card_num: int,
        target_player_num: int,
        target_card_num: int,
    ):
        """"""
        super().__init__(player_num)
        self.card_num = card_num
        self.target_player_num = target_player_num
        self.target_card_num = target_card_num

    def check_legality(self, state: State) -> None:
        """"""
        #
        if state.current_phase != Phase.CARD_STACKED:
            raise WrongPhaseError("No cards have been stacked yet", player_num=self.player_num)
        #
        if self.player_num in state.ended_turn_players:
            raise LegalityError(
                "You have already finished play for this turn",
                player_num=self.player_num,
            )
        #
        if self.player_num == state.shouf_player:
            raise LegalityError(
                "You've called Shouf, you can't make any moves",
                player_num=self.player_num,
            )
        #
        if self.target_player_num == state.shouf_player:
            raise LegalityError("That player's cards are Shouf locked", player_num=self.player_num)
        #
        if self.player_num != state.stacked_player:
            raise LegalityError(
                "Only the player who played stacked cards may move cards",
                player_num=self.player_num,
            )
        #
        if self.player_num == self.target_player_num:
            raise LegalityError(
                "You may not change card positions within your own hand",
                player_num=self.player_num,
            )
        #
        if (self.target_player_num, self.target_card_num) not in state.stacked_previous_targets:
            raise LegalityError(
                (
                    "You did not stack "
                    f"player {self.target_player_num}'s card {self.target_card_num}, "
                    "unable to move card to that position"
                ),
                player_num=self.player_num,
            )

    def apply(self, state: State) -> State:
        """"""
        new_state = state.copy()
        #
        player_pile = new_state.player_piles[self.player_num]
        target_player_pile = new_state.player_piles[self.target_player_num]
        #
        if self.card_num not in player_pile:
            raise ConsistencyError(
                f"Player {self.player_num} does not have a face-down card "
                f"in slot {self.card_num}",
            )
        if self.target_card_num in target_player_pile:
            raise ConsistencyError(
                f"Player {self.target_player_num} already has a face-down card "
                f"in slot {self.target_card_num}",
            )
        #
        target_player_pile[self.target_card_num] = player_pile[self.card_num]
        del player_pile[self.card_num]
        #
        new_state.stacked_previous_targets.remove((self.target_player_num, self.target_card_num))
        #
        return new_state

    def notify(self, listener: ActionListener) -> None:
        """"""
        listener.notify_move_card(
            player_num=self.player_num,
            card_num=self.card_num,
            target_player_num=self.target_player_num,
            target_card_num=self.target_card_num,
        )

    def __str__(self) -> str:
        """"""
        return (
            "MoveCardAction("
            f"player_num={self.player_num}, "
            f"card_num={self.card_num}, "
            f"target_player_num={self.target_player_num}, "
            f"target_card_num={self.target_card_num}"
            ")"
        )


class EndTurnAction(PostplayAction):
    """"""

    def check_legality(self, state: State) -> None:
        """"""
        if state.current_phase < Phase.CARD_EFFECT_USED:
            raise WrongPhaseError(
                "A play move needs to be made before you can end the turn",
                player_num=self.player_num,
            )
        if self.player_num in state.ended_turn_players:
            raise LegalityError(
                "You have already finished play for this turn",
                player_num=self.player_num,
            )
        if self.player_num == state.stacked_player and state.stacked_previous_targets:
            raise LegalityError(
                "You have stacked cards that you haven't replaced yet",
                player_num=self.player_num,
            )

    def apply(self, state: State) -> State:
        """"""
        new_state = state.copy()
        #
        new_state.ended_turn_players.add(self.player_num)
        if sorted(new_state.ended_turn_players) != new_state.player_nums:
            return new_state
        #
        player_nums = sorted(new_state.player_piles.keys())
        current_player_index = player_nums.index(new_state.current_player)
        new_current_player = player_nums[(current_player_index + 1) % len(player_nums)]
        # If one of the players called Shouf, and all players have had their final turn,
        # end the game.
        if new_state.shouf_player == new_current_player:
            new_state.current_phase = Phase.END_GAME
        else:
            # If one of the players has run out of face-down cards, end the game.
            for player_pile in new_state.player_piles.values():
                if not player_pile:
                    new_state.current_phase = Phase.END_GAME
                    break
            # No endgame conditions have been met. Start the next turn.
            else:
                new_state.current_turn += 1
                new_state.current_phase = Phase.NEW_TURN
                new_state.current_player = new_current_player
                new_state.drew_from_deck = False
                new_state.drew_from_discard = False
                new_state.stacked_player = None
                new_state.stacked_previous_targets = []
                new_state.ended_turn_players = set()
        #
        return new_state

    def notify(self, listener: ActionListener) -> None:
        """"""
        listener.notify_end_turn(player_num=self.player_num)

    def __str__(self) -> str:
        """"""
        return f"EndTurnAction(player_num={self.player_num})"
