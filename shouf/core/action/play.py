# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

from shouf.core.state import State, Phase

from shouf.core.action.base import PlayerAction
from shouf.core.action.listener.base import ActionListener

from shouf.core.exceptions import LegalityError, WrongPhaseError


class PlayAction(PlayerAction):
    """"""

    pass


class SwitchCardsAction(PlayAction):
    """"""

    def __init__(self, player_num: int, card_num: int):
        """"""
        super().__init__(player_num)
        self.card_num = card_num

    def check_legality(self, state: State) -> None:
        """"""
        if self.player_num != state.current_player:
            raise LegalityError(
                "You cannot switch cards when it is not your turn",
                player_num=self.player_num,
            )
        if self.player_num == state.shouf_player:
            raise LegalityError(
                "You've called Shouf, you can't make any moves",
                player_num=self.player_num,
            )
        if state.current_phase >= Phase.CARD_USED:
            raise WrongPhaseError(
                "You have already used a card this turn",
                player_num=self.player_num,
            )
        if state.current_phase != Phase.CARD_DRAWN:
            raise WrongPhaseError("You have not drawn a card yet", player_num=self.player_num)

    def apply(self, state: State) -> State:
        """"""
        new_state = state.copy()
        #
        hand_card = new_state.hand_card
        replaced_card = new_state.player_piles[self.player_num][self.card_num]
        #
        new_state.hand_card = None
        new_state.player_piles[self.player_num][self.card_num] = hand_card
        new_state.discard_pile.place(replaced_card)
        new_state.current_phase = Phase.CARD_EFFECT_USED
        #
        return new_state

    def notify(self, listener: ActionListener) -> None:
        """"""
        listener.notify_switch_cards(player_num=self.player_num, card_num=self.card_num)

    def __str__(self) -> str:
        """"""
        return f"SwitchCardsAction(player_num={self.player_num}, card_num={self.card_num})"


class PlayCardAction(PlayAction):
    """"""

    def check_legality(self, state: State) -> None:
        """"""
        if self.player_num != state.current_player:
            raise LegalityError(
                "You cannot switch cards when it is not your turn",
                player_num=self.player_num,
            )
        if self.player_num == state.shouf_player:
            raise LegalityError(
                "You've called Shouf, you can't make any moves",
                player_num=self.player_num,
            )
        if state.current_phase >= Phase.CARD_USED:
            raise WrongPhaseError(
                "You have already used a card this turn",
                player_num=self.player_num,
            )
        if state.current_phase != Phase.CARD_DRAWN:
            raise WrongPhaseError("You have not drawn a card yet", player_num=self.player_num)

    def apply(self, state: State) -> State:
        """"""
        new_state = state.copy()
        hand_card = new_state.hand_card
        new_state.discard_pile.place(hand_card)
        new_state.hand_card = None
        new_state.current_phase = (
            Phase.CARD_USED if hand_card.has_effect() else Phase.CARD_EFFECT_USED
        )
        return new_state

    def notify(self, listener: ActionListener) -> None:
        """"""
        listener.notify_play_card(player_num=self.player_num)

    def __str__(self) -> str:
        """"""
        return f"PlayCardAction(player_num={self.player_num})"
