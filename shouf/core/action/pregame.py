# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

import random

from shouf.core.card_pile import CardPile
from shouf.core.state import State, Phase

from shouf.core.action.base import Action
from shouf.core.action.listener.base import ActionListener

from shouf.core.exceptions import ConsistencyError, WrongPhaseError


class PregameAction(Action):
    """"""

    pass


class NewGameAction(PregameAction):
    """"""

    def __init__(self, starting_player_num: int):
        """"""
        self.starting_player_num = starting_player_num

    def check_legality(self, state: State) -> None:
        """"""
        if state.current_game > 0 and state.current_phase != Phase.END_GAME:
            raise WrongPhaseError("The current game has not ended")

    def apply(self, state: State) -> State:
        """"""
        new_state = state.copy()
        #
        new_state.current_game += 1
        if new_state.current_game > 1:
            for player_num, score in new_state.player_scores.items():
                new_state.previous_scores[player_num].append(score)
        #
        new_state.deck = CardPile(with_jokers=False)
        new_state.discard_pile = CardPile(start_empty=True)
        new_state.hand_card = None
        new_state.player_piles = {player_num: {} for player_num in new_state.player_nums}
        #
        new_state.current_turn = 0
        new_state.current_phase = Phase.NEW_GAME
        #
        new_state.current_player = self.starting_player_num
        new_state.shouf_player = None
        #
        new_state.drew_from_deck = False
        new_state.drew_from_discard = False
        #
        new_state.stacked_player = None
        new_state.stacked_previous_targets = []
        #
        return new_state

    def notify(self, listener: ActionListener) -> None:
        """"""
        listener.notify_new_game()

    def __str__(self) -> str:
        """"""
        return "NewGameAction()"


class ShuffleDeckAction(PregameAction):
    """"""

    def __init__(self, rand: random.Random):
        """"""
        self.shuffled_deck = CardPile(with_jokers=False)
        self.shuffled_deck.shuffle(rand=rand)

    def check_legality(self, state: State) -> None:
        """"""
        if state.current_phase < Phase.NEW_GAME:
            raise WrongPhaseError("A new game hasn't been setup yet")
        if state.current_phase >= Phase.DECK_SHUFFLED:
            raise WrongPhaseError("The deck has already been shuffled")

    def apply(self, state: State) -> State:
        """"""
        new_state = state.copy()
        new_state.deck = self.shuffled_deck.copy()
        new_state.current_phase = Phase.DECK_SHUFFLED
        return new_state

    def notify(self, listener: ActionListener) -> None:
        """"""
        listener.notify_shuffle_deck(self.shuffled_deck)

    def __str__(self) -> str:
        """"""
        return "ShuffleDeckAction()"


class DealCardAction(PregameAction):
    """"""

    def __init__(self, player_num: int, card_num: int):
        """"""
        self.player_num = player_num
        self.card_num = card_num

    def check_legality(self, state: State) -> None:
        """"""
        pass

    def apply(self, state: State) -> State:
        """"""
        new_state = state.copy()
        if self.card_num in new_state.player_piles[self.player_num]:
            raise ConsistencyError(
                f"Attempted to deal card {self.card_num} to player {self.player_num}, "
                f"player {self.player_num} already has that card",
            )
        new_state.player_piles[self.player_num][self.card_num] = new_state.deck.draw()
        return new_state

    def notify(self, listener: ActionListener) -> None:
        """"""
        listener.notify_deal_card(self.player_num, self.card_num)

    def __str__(self) -> str:
        """"""
        return f"DealCardAction(player_num={self.player_num}, card_num={self.card_num})"


class FinishDealingCardsAction(PregameAction):
    """"""

    def check_legality(self, state: State) -> None:
        """"""
        if state.current_phase != Phase.DECK_SHUFFLED:
            raise WrongPhaseError(
                "You can only deal cards if the deck has been freshly shuffled",
            )

    def apply(self, state: State) -> State:
        """"""
        new_state = state.copy()
        new_state.current_phase = Phase.CARDS_DEALED
        return new_state

    def notify(self, listener: ActionListener) -> None:
        """"""
        listener.notify_finish_dealing_cards()

    def __str__(self) -> str:
        """"""
        return "FinishDealingCardsAction()"


class ShowFrontTwoCardsAction(PregameAction):
    """"""

    def check_legality(self, state: State) -> None:
        """"""
        if state.started:
            raise WrongPhaseError(
                "The game has already started, you cannot look at your face-down cards",
            )
        if state.current_phase >= Phase.SHOWN_FRONT_TWO_CARDS:
            raise WrongPhaseError("You have already looked at your front two cards")
        if state.current_phase < Phase.CARDS_DEALED:
            raise WrongPhaseError("Cards have not been dealed to players yet")

    def apply(self, state: State) -> State:
        """"""
        new_state = state.copy()
        new_state.current_phase = Phase.SHOWN_FRONT_TWO_CARDS
        return new_state

    def notify(self, listener: ActionListener) -> None:
        """"""
        listener.notify_show_front_two_cards()

    def __str__(self) -> str:
        """"""
        return "ShowFrontTwoCardsAction()"


class StartGameAction(PregameAction):
    """"""

    def check_legality(self, state: State) -> None:
        """"""
        if state.started:
            raise WrongPhaseError("The game has already started")

    def apply(self, state: State) -> State:
        """"""
        new_state = state.copy()
        new_state.current_phase = Phase.NEW_TURN
        new_state.current_turn = 1
        return new_state

    def notify(self, listener: ActionListener) -> None:
        """"""
        listener.notify_start_game()

    def __str__(self) -> str:
        """"""
        return "StartGameAction()"
