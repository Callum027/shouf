# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

from typing import TYPE_CHECKING

from shouf.core.action.preplay import CallShoufAction, DrawDeckAction, DrawDiscardAction
from shouf.core.action.play import SwitchCardsAction, PlayCardAction
from shouf.core.action.card_effect import SwapPlayersCardsAction, ShowCardAction, NoCardEffectAction
from shouf.core.action.postplay import StackCardAction, MoveCardAction, EndTurnAction

if TYPE_CHECKING:
    from shouf.core.engine import Engine


class Controller:
    def __init__(self, engine: Engine, player_num: int):
        """"""
        self.engine = engine
        self.player_num = player_num

    def draw_from_deck(self) -> None:
        """"""
        self.engine.trigger_action(DrawDeckAction(player_num=self.player_num))

    def draw_from_discard(self) -> None:
        """"""
        self.engine.trigger_action(DrawDiscardAction(player_num=self.player_num))

    def switch_cards(self, card_num: int) -> None:
        """"""
        self.engine.trigger_action(
            SwitchCardsAction(player_num=self.player_num, card_num=card_num),
        )

    def play_card(self) -> None:
        """"""
        self.engine.trigger_action(PlayCardAction(player_num=self.player_num))

    def swap_players_cards(
        self,
        card_num: int,
        other_player_num: int,
        other_card_num: int,
        show_cards: bool,
    ) -> None:
        """"""
        self.engine.trigger_action(
            SwapPlayersCardsAction(
                player_num=self.player_num,
                card_num=card_num,
                other_player_num=other_player_num,
                other_card_num=other_card_num,
                show_cards=show_cards,
            ),
        )

    def show_own_card(self, card_num: int) -> None:
        """"""
        self.engine.trigger_action(
            ShowCardAction(
                player_num=self.player_num,
                target_player_num=self.player_num,
                target_card_num=card_num,
            ),
        )

    def show_other_player_card(self, target_player_num: int, target_card_num: int) -> None:
        """"""
        if target_player_num == self.player_num:
            raise ValueError("show_other_player_card cannot be used to show a player's own cards")
        self.engine.trigger_action(
            ShowCardAction(
                player_num=self.player_num,
                target_player_num=target_player_num,
                target_card_num=target_card_num,
            ),
        )

    def no_card_effect(self) -> None:
        """"""
        self.engine.trigger_action(NoCardEffectAction(player_num=self.player_num))

    def stack_card(self, target_player_num: int, target_card_num: int) -> None:
        """"""
        self.engine.trigger_action(
            StackCardAction(
                player_num=self.player_num,
                target_player_num=target_player_num,
                target_card_num=target_card_num,
            ),
        )

    def move_card(self, card_num: int, target_player_num: int, target_card_num: int) -> None:
        """"""
        self.engine.trigger_action(
            MoveCardAction(
                player_num=self.player_num,
                card_num=card_num,
                target_player_num=target_player_num,
                target_card_num=target_card_num,
            ),
        )

    def call_shouf(self) -> None:
        """"""
        self.engine.trigger_action(CallShoufAction(self.player_num))

    def end_turn(self) -> None:
        """"""
        self.engine.trigger_action(EndTurnAction(self.player_num))
