# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

import random

from typing import overload, Iterator, List, Optional, Sequence, Union

from shouf.core.card import Card, Suit, Rank

from shouf.util import Seed


class CardPile(Sequence[Card]):
    """"""

    def __init__(
        self,
        start_empty: bool = False,
        with_jokers: bool = False,
        pile: Optional[List[Card]] = None,
    ):
        """"""
        #
        self.start_empty = start_empty
        self.with_jokers = with_jokers
        #
        if pile is not None:
            self.pile = pile
        else:
            self.pile = []
            self.reset()

    def reset(self) -> None:
        """"""
        new_pile: List[Card] = []
        #
        if self.start_empty:
            self.pile = new_pile
            return
        #
        for suit in Suit:
            for rank in Rank:
                if rank == Rank.JOKER:
                    if not self.with_jokers:
                        continue
                    if suit not in (Suit.BLACK, Suit.RED):
                        continue
                elif suit in (Suit.BLACK, Suit.RED):
                    continue
                new_pile.append(Card(suit, rank))
        #
        self.pile = new_pile

    def shuffle(self, rand: Optional[random.Random] = None, seed: Seed = None) -> None:
        """"""
        if not rand:
            rand = random.Random(seed)
        self.pile = rand.sample(self.pile, k=len(self.pile))

    def get_top_card(self) -> Card:
        """"""
        return self.pile[0]

    def get_top_cards(self, num_cards: int = 1) -> List[Card]:
        """"""
        if not self.pile:
            return []
        if num_cards > len(self.pile):
            num_cards = len(self.pile)
        return self.pile[0:num_cards]

    def draw(self) -> Card:
        """"""
        drawn_card = self.pile[0]
        if len(self.pile) > 1:
            self.pile = self.pile[1:]
        else:
            self.pile = []
        return drawn_card

    def place(self, card: Card) -> None:
        """"""
        self.pile = [card] + self.pile

    def copy(self) -> "CardPile":
        """"""
        return CardPile(
            start_empty=self.start_empty,
            with_jokers=self.with_jokers,
            pile=[card.copy() for card in self.pile],
        )

    @overload
    def __getitem__(self, key: int) -> Card:
        ...

    @overload
    def __getitem__(self, key: slice) -> "CardPile":
        ...

    def __getitem__(self, key: Union[int, slice]) -> Union[Card, "CardPile"]:
        """"""
        if isinstance(key, int):
            return self.pile[key]
        return CardPile(
            start_empty=self.start_empty,
            with_jokers=self.with_jokers,
            pile=self.pile[key],
        )

    def __len__(self) -> int:
        """"""
        return len(self.pile)

    def __iter__(self) -> Iterator[Card]:
        """"""
        return iter(self.pile)

    def __reversed__(self) -> Iterator[Card]:
        """"""
        return reversed(self.pile)

    def __str__(self) -> str:
        """"""
        output = "[\n" if self.pile else "["
        for card in self.pile:
            output += f"    {card},\n"
        output += "]"
        return output
