# -*- coding: utf-8 -*-

"""
Shouf game engine.
"""


from __future__ import annotations

import os
import random

from typing import TYPE_CHECKING, Mapping, Set

from shouf.core.action.pregame import (
    NewGameAction,
    ShuffleDeckAction,
    DealCardAction,
    FinishDealingCardsAction,
    ShowFrontTwoCardsAction,
    StartGameAction,
)

from shouf.core.state import State, Phase

from shouf.util import Seed

if TYPE_CHECKING:
    from shouf.core.action.base import Action
    from shouf.core.action.listener.base import ActionListener
    from shouf.core.player.base import Player


class Engine:
    """"""

    def __init__(
        self,
        players: Mapping[int, Player],
        seed: Seed = None,
        starting_num_cards: int = 4,
    ):
        """"""
        #
        self.players = {player_num: player.copy() for player_num, player in players.items()}
        self.seed = seed if seed is not None else os.urandom(32)
        self.starting_num_cards = starting_num_cards
        #
        self.random = random.Random(seed)
        #
        self.player_nums = sorted(self.players.keys())
        #
        self.current_state = State(player_nums=self.player_nums)
        #
        self.action_listeners: Set[ActionListener] = set()

    def register_action_listener(self, listener: ActionListener) -> None:
        """"""
        self.action_listeners = self.action_listeners.union(set([listener]))
        listener.subscribe_to_engine(self)

    def deregister_action_listeners(self) -> None:
        """"""
        self.action_listeners = set()

    def trigger_action(self, action: Action) -> None:
        """"""
        # print(f"Triggering action: {action}")
        action.check_legality(self.current_state)
        self.current_state = action.apply(self.current_state)
        for listener in self.action_listeners:
            action.notify(listener)
        #
        if self.current_state.current_phase == Phase.END_GAME:
            self.deregister_action_listeners()

    def new_game(self) -> None:
        """"""
        if not self.current_state.current_game:
            starting_player_num = self.random.choice(self.player_nums)
        else:
            losing_players = self.current_state.losing_players
            if len(losing_players) == 1:
                starting_player_num = losing_players[0]
            elif losing_players:
                starting_player_num = self.random.choice(losing_players)
            else:
                raise ValueError(f"Invalid state for losing_players: {losing_players}")
        self.trigger_action(NewGameAction(starting_player_num))

    def shuffle_deck(self) -> None:
        """"""
        self.trigger_action(ShuffleDeckAction(self.random))

    def deal_cards(self) -> None:
        """"""
        for card_num in range(1, self.starting_num_cards + 1):
            for player_num in self.player_nums:
                self.trigger_action(DealCardAction(player_num, card_num))
        self.trigger_action(FinishDealingCardsAction())

    def show_front_two_cards(self) -> None:
        """"""
        self.trigger_action(ShowFrontTwoCardsAction())

    def start_game(self) -> None:
        """"""
        self.trigger_action(StartGameAction())
