# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

import enum


class Suit(enum.IntEnum):
    """"""

    CLUBS = 1
    SPADES = 2
    HEARTS = 3
    DIAMONDS = 4
    BLACK = 5
    RED = 6

    def is_black(self) -> bool:
        """"""
        return self in (Suit.CLUBS, Suit.SPADES, Suit.BLACK)

    def is_red(self) -> bool:
        """"""
        return self in (Suit.HEARTS, Suit.DIAMONDS, Suit.RED)


class Rank(enum.IntEnum):
    """"""

    ACE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5
    SIX = 6
    SEVEN = 7
    EIGHT = 8
    NINE = 9
    TEN = 10
    JACK = 11
    QUEEN = 12
    KING = 13
    JOKER = 14


class Card:
    """"""

    def __init__(self, suit: Suit, rank: Rank):
        """"""
        #
        self.suit = suit
        self.rank = rank
        #
        if self.rank == Rank.JOKER:
            if self.suit not in (Suit.BLACK, Suit.RED):
                raise ValueError(f"Invalid suit for {self.rank.name}: must be either BLACK or RED")
        else:
            if self.suit not in (Suit.CLUBS, Suit.SPADES, Suit.HEARTS, Suit.DIAMONDS):
                raise ValueError(
                    f"Invalid suit for {self.rank.name}: "
                    "must be one of (CLUBS, SPADES, HEARTS, DIAMONDS)",
                )

    @property
    def description(self) -> str:
        """"""
        if self.suit in (Suit.BLACK, Suit.RED):
            return f"{self.suit.name.capitalize()} {self.rank.name.capitalize()}"
        return f"{self.rank.name.capitalize()} of {self.suit.name.capitalize()}"

    @property
    def points(self) -> int:
        """"""
        if self.rank == Rank.JOKER:
            return 0
        elif self.rank == Rank.KING:
            return 35 if self.suit.is_black() else -1
        elif self.rank in (Rank.QUEEN, Rank.JACK):
            return 10
        return int(self.rank)

    def has_effect(self) -> bool:
        """"""
        if self.rank in (Rank.SEVEN, Rank.EIGHT, Rank.NINE, Rank.TEN, Rank.JACK, Rank.QUEEN):
            return True
        if self.rank == Rank.KING and self.suit.is_black():
            return True
        return False

    def copy(self) -> "Card":
        """"""
        return Card(suit=self.suit, rank=self.rank)

    def __str__(self) -> str:
        """"""
        return f"Card({self.suit.name}, {self.rank.name})"
