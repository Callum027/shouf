# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations


class Config:
    graphics_resolution = "800x600"
    graphics_fullscreen = False


config = Config()
