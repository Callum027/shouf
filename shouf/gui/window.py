# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

import signal
import sys

from typing import List

import arcade
import pyautogui

from shouf.config import config

from shouf.computer import base as computer_base

from shouf.gui.view.start import StartView

SCREEN_TITLE = "Shouf"

DEFAULT_WINDOWED_WIDTH = 800
DEFAULT_WINDOWED_HEIGHT = 600


class ShoufWindow(arcade.Window):
    """"""

    def __init__(self):
        """"""
        if config.graphics_resolution:
            width, height = config.graphics_resolution.split("x")
            screen_width = int(width)
            screen_height = int(height)
        else:
            if config.graphics_fullscreen:
                width, height = pyautogui.size()
            else:
                width = DEFAULT_WINDOWED_WIDTH
                height = DEFAULT_WINDOWED_HEIGHT
        super().__init__(
            screen_width,
            screen_height,
            SCREEN_TITLE,
            fullscreen=config.graphics_fullscreen,
            resizable=True,
        )

    def setup(self):
        """
        Set up the game here. Call this function to restart the game.
        """
        #
        self.computers: List["computer_base.Computer"] = []
        #
        signal.signal(signal.SIGTERM, self.signal_handler)
        signal.signal(signal.SIGINT, self.signal_handler)

    def close(self) -> None:
        """"""
        print("Closing window")
        self.stop_computers()
        super().close()

    def signal_handler(self, signum, frame):
        print("Caught signal, exiting")
        self.stop_computers()

    def stop_computers(self):
        """"""
        for computer in self.computers:
            computer.stop()
        sys.exit(0)


def main():
    """
    Main method
    """

    window = ShoufWindow()
    start_view = StartView()
    start_view.setup()
    window.show_view(start_view)
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()
