# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

from shouf.core.card import Suit, Rank


CARD_SCALE = 0.6
CARD_FACEUP_IMAGE = ":resources:images/cards/card{suit}{rank}.png"
CARD_FACEDOWN_IMAGE = ":resources:images/cards/cardBack_red2.png"
CARD_RANK_SPRITE_MAP = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "Joker"]


def get_card_faceup_image(suit: Suit, rank: Rank) -> str:
    """"""

    return CARD_FACEUP_IMAGE.format(
        suit=suit.name.capitalize() if rank != Rank.JOKER else "",
        rank=CARD_RANK_SPRITE_MAP[rank.value - 1],
    )
