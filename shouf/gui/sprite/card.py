# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

import itertools

from typing import Iterator, List, Union

import arcade

from shouf.core.card import Card, Suit, Rank

from shouf.gui.util import get_card_faceup_image, CARD_FACEDOWN_IMAGE


CARD_FACEUP_TEXTURES = {
    (suit, rank): arcade.load_texture(get_card_faceup_image(suit, rank))
    for suit, rank in itertools.product(
        [Suit.CLUBS, Suit.SPADES, Suit.HEARTS, Suit.DIAMONDS],
        [
            Rank.ACE,
            Rank.TWO,
            Rank.THREE,
            Rank.FOUR,
            Rank.FIVE,
            Rank.SIX,
            Rank.SEVEN,
            Rank.EIGHT,
            Rank.NINE,
            Rank.TEN,
            Rank.JACK,
            Rank.QUEEN,
            Rank.KING,
        ],
    )
}
CARD_FACEUP_TEXTURES[Suit.BLACK, Rank.JOKER] = arcade.load_texture(
    get_card_faceup_image(Suit.BLACK, Rank.JOKER),
)
CARD_FACEUP_TEXTURES[Suit.RED, Rank.JOKER] = arcade.load_texture(
    get_card_faceup_image(Suit.RED, Rank.JOKER),
)
CARD_FACEDOWN_TEXTURE = arcade.load_texture(CARD_FACEDOWN_IMAGE)

#
CARD_WIDTH = CARD_FACEDOWN_TEXTURE.width
CARD_HEIGHT = CARD_FACEDOWN_TEXTURE.height
CARD_SIZE = (CARD_WIDTH, CARD_HEIGHT)
CARD_GAP = 24


class CardSprite(arcade.Sprite):
    """"""

    def __init__(self, card: Card, faceup: bool = False, **kwargs):
        """"""
        super().__init__(**kwargs)
        self.card = card
        self.faceup_texture = CARD_FACEUP_TEXTURES[(self.card.suit, self.card.rank)]
        self.facedown_texture = CARD_FACEDOWN_TEXTURE
        self.is_faceup = faceup
        self.texture = self.faceup_texture if self.is_faceup else self.facedown_texture

    @property
    def is_facedown(self) -> bool:
        """"""
        return not self.is_faceup

    def set_faceup(self) -> bool:
        """"""
        was_faceup = self.is_faceup
        self.is_faceup = True
        self.texture = self.faceup_texture
        return was_faceup

    def set_facedown(self) -> bool:
        """"""
        was_facedown = not self.is_faceup
        self.is_faceup = False
        self.texture = self.facedown_texture
        return was_facedown

    def flip(self) -> bool:
        """"""
        was_faceup = self.is_faceup
        if self.is_faceup:
            self.set_faceup()
        else:
            self.set_facedown()
        return was_faceup

    def __str__(self) -> str:
        """"""
        return f"CardSprite({self.card}, faceup={self.is_faceup})"


class CardSpriteList:
    """"""

    def __init__(self, *args, **kwargs):
        """"""
        self.sprite_list = arcade.SpriteList(*args, **kwargs)

    def __len__(self) -> int:
        """"""
        return len(self.sprite_list)

    def __iter__(self) -> Iterator[CardSprite]:
        """"""
        return iter(self.sprite_list)

    def __getitem__(self, i: int) -> CardSprite:
        """"""
        return self.sprite_list[i]

    def __setitem__(self, key: int, value: CardSprite) -> None:
        """"""
        self.sprite_list[key] = value

    def index(self, key):
        """"""
        return self.sprite_list.index(key)

    def pop(self, index: int = -1) -> CardSprite:
        """"""
        return self.sprite_list.pop(index)

    def append(self, item: CardSprite) -> None:
        """"""
        self.sprite_list.append(item)

    def extend(self, items: Union[List[CardSprite], "CardSpriteList"]):
        """"""
        self.sprite_list.extend(items)

    def insert(self, index: int, item: CardSprite) -> None:
        """"""
        self.sprite_list.insert(index, item)

    def remove(self, item: CardSprite) -> None:
        """"""
        self.sprite_list.remove(item)

    def reverse(self) -> None:
        """"""
        self.sprite_list.reverse()

    def update(self) -> None:
        """"""
        self.sprite_list.update()

    def rescale(self, factor: float) -> None:
        """"""
        self.sprite_list.rescale(factor)

    def move(self, change_x: float, change_y: float) -> None:
        """"""
        self.sprite_list.move(change_x, change_y)

    def draw(self, **kwargs) -> None:
        """"""
        self.sprite_list.draw(**kwargs)
