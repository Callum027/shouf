# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

from typing import TYPE_CHECKING, List, Dict

import arcade
import arcade.gui

from shouf import computer

from shouf.core.action.listener.logger import ActionLogger

from shouf.core.player.computer import ComputerPlayer
from shouf.core.player.local import LocalPlayer

from shouf.gui.view.game import GameView

if TYPE_CHECKING:
    from shouf.computer.base import Computer
    from shouf.core.engine import Engine
    from shouf.core.player.base import Player


class LoadGameView(arcade.View):
    """"""

    def __init__(self, engine: Engine, my_player_num: int):
        """"""
        #
        self.engine = engine
        self.my_player_num = my_player_num
        # Call the Arcade View initialisation code.
        super().__init__()

    @property
    def players(self) -> Dict[int, Player]:
        """"""
        return self.engine.players

    @property
    def my_player(self) -> LocalPlayer:
        """"""
        player = self.players[self.my_player_num]
        if not isinstance(player, LocalPlayer):
            raise ValueError(f"Player {self.my_player_num} is not a local player")
        return player

    @property
    def computer_players(self) -> Dict[int, ComputerPlayer]:
        """"""
        return {
            player_num: player
            for player_num, player in self.players.items()
            if isinstance(player, ComputerPlayer)
        }

    def on_show_view(self) -> None:
        """"""
        #
        logger = ActionLogger()
        self.engine.register_action_listener(logger)
        #
        computers: List[Computer] = []
        for player in self.computer_players.values():
            comp = computer.create(player)
            computers.append(comp)
            self.engine.register_action_listener(comp)
            comp.start()
        self.window.computers = computers
        #
        game_view = GameView(self.my_player)
        self.engine.register_action_listener(game_view)
        game_view.setup()
        #
        self.engine.new_game()
        #
        self.window.show_view(game_view)
