# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

from typing import TYPE_CHECKING, Any, Dict

import arcade
import arcade.gui

from shouf.computer import Difficulty

from shouf.core.engine import Engine

from shouf.core.player.local import LocalPlayer
from shouf.core.player.computer import ComputerPlayer

from shouf.gui.view.load_game import LoadGameView

if TYPE_CHECKING:
    from shouf.core.player.base import Player


class StartView(arcade.View):
    """"""

    def __init__(self):
        """"""
        self.player_name = "Player 1"
        self.num_human_players = 1
        self.num_computer_players = 3
        self.computer_difficulty = Difficulty.HARD
        self.ui_manager = arcade.gui.UIManager(attach_callbacks=False)
        self.ui_manager.register_handlers()
        super().__init__()

    def setup(self):
        """"""
        #
        self.ui_manager.register_handlers()

    def on_show_view(self):
        """ Called once when view is activated. """
        #
        arcade.set_background_color(arcade.color.AMAZON)
        #
        # self.ui_manager.unregister_handlers()
        self.ui_manager.purge_ui_elements()
        #
        self.player_name_box = arcade.gui.UIInputBox(
            center_x=self.item_center_x,
            center_y=self.player_name_center_y,
            width=self.player_name_box_width,
        )
        self.player_name_box.text = self.player_name
        self.player_name_box.cursor_index = len(self.player_name_box.text)
        self.ui_manager.add_ui_element(self.player_name_box)
        #
        self.decrease_num_computer_players_button = DecreaseValueButton(
            self,
            "num_computer_players",
            lower_limit=1,
            center_x=self.decrease_button_center_x,
            center_y=self.num_computer_players_center_y,
            width=self.increase_decrease_button_size,
            height=self.increase_decrease_button_size,
        )
        self.ui_manager.add_ui_element(self.decrease_num_computer_players_button)
        self.increase_num_computer_players_button = IncreaseValueButton(
            self,
            "num_computer_players",
            higher_limit=3,
            center_x=self.increase_button_center_x,
            center_y=self.num_computer_players_center_y,
            width=self.increase_decrease_button_size,
            height=self.increase_decrease_button_size,
        )
        self.ui_manager.add_ui_element(self.increase_num_computer_players_button)
        #
        self.start_button = StartButton(
            self.window,
            self,
            "Start Game",
            center_x=self.window.width / 2,
            center_y=self.window.height * 0.25,
            width=250,
            # height=20
        )
        self.ui_manager.add_ui_element(self.start_button)
        #
        self.decrease_computer_difficulty_button = DecreaseValueButton(
            self,
            "computer_difficulty",
            lower_limit=Difficulty.EASY,
            center_x=self.decrease_button_center_x,
            center_y=self.computer_difficulty_center_y,
            width=self.increase_decrease_button_size,
            height=self.increase_decrease_button_size,
        )
        self.ui_manager.add_ui_element(self.decrease_computer_difficulty_button)
        self.increase_computer_difficulty_button = IncreaseValueButton(
            self,
            "computer_difficulty",
            higher_limit=Difficulty.HARD,
            center_x=self.increase_button_center_x,
            center_y=self.computer_difficulty_center_y,
            width=self.increase_decrease_button_size,
            height=self.increase_decrease_button_size,
        )
        self.ui_manager.add_ui_element(self.increase_computer_difficulty_button)
        #
        # self.ui_manager.register_handlers()

    def on_hide_view(self):
        """"""
        # self.ui_manager.purge_ui_elements()
        print("StartView.on_hide_view")
        self.ui_manager.purge_ui_elements()
        self.ui_manager.unregister_handlers()

    def on_draw(self):
        """"""
        #
        arcade.start_render()
        #
        arcade.draw_text(
            "Player Name",
            start_x=self.text_right_x,
            start_y=self.player_name_center_y,
            color=arcade.color.WHITE,
            font_size=28,
            anchor_x="right",
            anchor_y="center",
        )
        #
        arcade.draw_text(
            "Number of Computer Players",
            start_x=self.text_right_x,
            start_y=self.num_computer_players_center_y,
            color=arcade.color.WHITE,
            font_size=28,
            anchor_x="right",
            anchor_y="center",
        )
        arcade.draw_text(
            str(self.num_computer_players),
            start_x=self.item_center_x,
            start_y=self.num_computer_players_center_y,
            color=arcade.color.WHITE,
            font_size=28,
            anchor_x="center",
            anchor_y="center",
        )
        #
        arcade.draw_text(
            "Computer Difficulty",
            start_x=self.text_right_x,
            start_y=self.computer_difficulty_center_y,
            color=arcade.color.WHITE,
            font_size=28,
            anchor_x="right",
            anchor_y="center",
        )
        arcade.draw_text(
            self.computer_difficulty.name.capitalize(),
            start_x=self.item_center_x,
            start_y=self.computer_difficulty_center_y,
            color=arcade.color.WHITE,
            font_size=28,
            anchor_x="center",
            anchor_y="center",
        )

    def on_resize(self, width, height):
        """"""
        #
        self.window.on_resize(width, height)
        #
        self.player_name_box.center_x = self.item_center_x
        self.player_name_box.center_y = self.player_name_center_y
        self.player_name_box.width = self.player_name_box_width
        #
        self.decrease_num_computer_players_button.center_x = self.decrease_button_center_x
        self.decrease_num_computer_players_button.center_y = self.num_computer_players_center_y
        self.decrease_num_computer_players_button.width = self.increase_decrease_button_size
        self.decrease_num_computer_players_button.height = self.increase_decrease_button_size
        #
        self.increase_num_computer_players_button.center_x = self.increase_button_center_x
        self.increase_num_computer_players_button.center_y = self.num_computer_players_center_y
        self.increase_num_computer_players_button.width = self.increase_decrease_button_size
        self.increase_num_computer_players_button.height = self.increase_decrease_button_size
        #
        self.decrease_computer_difficulty_button.center_x = self.decrease_button_center_x
        self.decrease_computer_difficulty_button.center_y = self.computer_difficulty_center_y
        self.decrease_computer_difficulty_button.width = self.increase_decrease_button_size
        self.decrease_computer_difficulty_button.height = self.increase_decrease_button_size
        #
        self.increase_computer_difficulty_button.center_x = self.increase_button_center_x
        self.increase_computer_difficulty_button.center_y = self.computer_difficulty_center_y
        self.increase_computer_difficulty_button.width = self.increase_decrease_button_size
        self.increase_computer_difficulty_button.height = self.increase_decrease_button_size
        #
        self.start_button.center_x = width / 2
        self.start_button.center_y = height * 0.25

    #
    #
    #

    @property
    def scale(self) -> float:
        """"""
        return ((self.window.width / 1920) + (self.window.height / 1080)) / 2

    @property
    def text_font_size(self) -> int:
        """"""
        return 28

    @property
    def text_margin(self) -> int:
        """"""
        return 16

    @property
    def item_y_margin(self) -> int:
        """"""
        return self.text_font_size + 16

    @property
    def player_name_box_width(self) -> int:
        """"""
        return 256

    @property
    def increase_decrease_button_size(self) -> int:
        """"""
        return 32

    @property
    def player_name_center_y(self) -> float:
        """"""
        return self.window.height / 2

    @property
    def num_computer_players_center_y(self) -> float:
        """"""
        return self.player_name_center_y - self.item_y_margin

    @property
    def computer_difficulty_center_y(self) -> float:
        """"""
        return self.num_computer_players_center_y - self.item_y_margin

    @property
    def text_right_x(self) -> float:
        """"""
        return (self.window.width / 2) - (self.text_margin / 2)

    @property
    def item_left_x(self) -> float:
        """"""
        return (self.window.width / 2) + (self.text_margin / 2)

    @property
    def item_center_x(self) -> float:
        """"""
        return self.item_left_x + (self.player_name_box_width / 2)

    @property
    def decrease_button_center_x(self) -> float:
        """"""
        return (
            (self.window.width / 2)
            + (self.text_margin / 2)
            + (self.increase_decrease_button_size / 2)
        )

    @property
    def increase_button_center_x(self) -> float:
        """"""
        return (
            (self.window.width / 2)
            + (self.text_margin / 2)
            + self.player_name_box_width
            - (self.increase_decrease_button_size / 2)
        )


class StartButton(arcade.gui.UIFlatButton):
    """"""

    def __init__(self, window: arcade.Window, view: StartView, *args, **kwargs):
        """"""
        self.window = window
        self.view = view
        super().__init__(*args, **kwargs)

    def on_click(self) -> None:
        """"""
        players: Dict[int, Player] = {1: LocalPlayer(num=1, name=self.view.player_name_box.text)}
        for player_num in range(2, 2 + self.view.num_computer_players):
            players[player_num] = ComputerPlayer(
                num=player_num,
                difficulty=self.view.computer_difficulty,
            )
        engine = Engine(players=players)
        load_game_view = LoadGameView(engine, my_player_num=1)
        self.window.show_view(load_game_view)


class DecreaseValueButton(arcade.gui.UIFlatButton):
    """"""

    def __init__(self, view: StartView, key: str, lower_limit: Any, *args, **kwargs):
        """"""
        self.view = view
        self.key = key
        self.lower_limit = lower_limit
        self.type = type(self.lower_limit)
        super().__init__("◄", *args, **kwargs)

    def on_click(self) -> None:
        """"""
        if getattr(self.view, self.key) > self.lower_limit:
            setattr(self.view, self.key, self.type(int(getattr(self.view, self.key)) - 1))


class IncreaseValueButton(arcade.gui.UIFlatButton):
    """"""

    def __init__(self, view: StartView, key: str, higher_limit: Any, *args, **kwargs):
        """"""
        self.view = view
        self.key = key
        self.higher_limit = higher_limit
        self.type = type(self.higher_limit)
        super().__init__("►", *args, **kwargs)

    def on_click(self) -> None:
        """"""
        if getattr(self.view, self.key) < self.higher_limit:
            setattr(self.view, self.key, self.type(int(getattr(self.view, self.key)) + 1))
