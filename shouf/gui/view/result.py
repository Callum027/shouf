# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

from typing import TYPE_CHECKING, Dict

import arcade
import arcade.gui

from shouf.core.card import Card

from shouf.gui.sprite.card import CardSprite, CARD_WIDTH, CARD_HEIGHT

from shouf.gui.view.total_score import TotalScoreView

if TYPE_CHECKING:
    from shouf.core.engine import Engine
    from shouf.core.player.base import Player


CARD_MARGIN_RATIO = 0.2
CARD_MAT_SIZE_RATIO = 0.1


class ResultView(arcade.View):
    """"""

    def __init__(self, engine: Engine, my_player_num: int):
        """"""
        #
        self.engine = engine
        self.my_player_num = my_player_num
        #
        self.state = self.engine.current_state
        #
        self.player_piles: Dict[int, Dict[int, CardSprite]] = {}
        #
        self.ui_manager = arcade.gui.UIManager(attach_callbacks=False)
        # Call the Arcade View initialisation code.
        super().__init__()

    @property
    def players(self) -> Dict[int, Player]:
        """"""
        return self.engine.players

    def setup(self) -> None:
        """"""
        self.ui_manager.register_handlers()

    def on_show_view(self):
        """"""
        #
        self.ui_manager.purge_ui_elements()
        # self.ui_manager.unregister_handlers()
        #
        self.total_score_button = TotalScoreButton(
            self,
            center_x=self.get_total_score_button_x(),
            center_y=self.get_total_score_button_y(),
        )
        self.ui_manager.add_ui_element(self.total_score_button)
        #
        if not self.player_piles:
            for player_num, pile in self.state.player_piles.items():
                self.player_piles[player_num] = {}
                for card_num, card in enumerate(pile.values(), 1):
                    self.player_piles[player_num][card_num] = self.create_card_sprite(
                        player_num,
                        card_num,
                        card,
                    )
        #
        # self.ui_manager.register_handlers()
        # self.setup()
        # arcade.set_background_color(arcade.color.AMAZON)

    def on_hide_view(self):
        """"""
        self.ui_manager.purge_ui_elements()
        self.ui_manager.unregister_handlers()

    def on_draw(self) -> None:
        """"""
        #
        arcade.start_render()
        #
        player_scores = self.state.player_scores
        winning_players = self.state.winning_players
        #
        if len(winning_players) == 1:
            players_win_text = f"{self.players[winning_players[0]].name} wins!"
        elif winning_players:
            players_win_text = (
                ", ".join(self.players[pn].name for pn in winning_players[:-1])
                + f" and {self.players[winning_players[-1]].name} win!"
            )
        else:
            raise ValueError(f"Invalid state for winning_players: {winning_players}")
        #
        for player_num, pile in self.player_piles.items():
            #
            arcade.draw_text(
                f"{self.players[player_num].name}:",
                start_x=self.get_player_text_x(),
                start_y=self.get_player_y(player_num),
                color=arcade.color.WHITE,
                font_size=28,
                anchor_x="right",
                anchor_y="center",
            )
            #
            for card in pile.values():
                card.draw()
            #
            score = player_scores[player_num]
            total_text = ""
            if not pile:
                total_text += "No Cards (0) "
            if player_num == self.state.shouf_player and player_num not in winning_players:
                total_text += "+ Shouf Penalty (25) "
            total_text += f"= {score} point{'s' if score != 1 else ''}"
            arcade.draw_text(
                total_text,
                start_x=self.get_score_text_x(len(pile)),
                start_y=self.get_player_y(player_num),
                color=arcade.color.WHITE,
                font_size=28,
                anchor_x="left",
                anchor_y="center",
            )
        #
        arcade.draw_text(
            players_win_text,
            start_x=self.window.width / 2,
            start_y=self.get_player_y(len(player_scores) + 1),
            color=arcade.color.WHITE,
            font_size=28,
            anchor_x="center",
            anchor_y="center",
        )

    def on_resize(self, width, height):
        """"""
        #
        self.window.on_resize(width, height)
        #
        for player_num, pile in self.player_piles.items():
            for card_num, card in pile.items():
                card.scale = self.card_scale
                card.center_x = self.get_card_x(card_num)
                card.center_y = self.get_player_y(player_num)
        #
        self.total_score_button.center_x = self.get_total_score_button_x()
        self.total_score_button.center_y = self.get_total_score_button_y()

    def create_card_sprite(self, player_num: int, card_num: int, card: Card) -> CardSprite:
        """"""
        return CardSprite(
            card,
            faceup=True,
            scale=self.card_scale,
            center_x=self.get_card_x(card_num),
            center_y=self.get_player_y(player_num),
        )

    #
    #
    #

    @property
    def scale(self) -> float:
        """"""
        width, height = self.window.get_size()
        return ((width / 1920) + (height / 1080)) / 2

    @property
    def card_scale(self) -> float:
        """"""
        return 0.5 * self.scale

    @property
    def card_width(self) -> float:
        """"""
        return CARD_WIDTH * self.card_scale

    @property
    def card_height(self) -> float:
        """"""
        return CARD_HEIGHT * self.card_scale

    @property
    def card_margin(self) -> float:
        """"""
        return (((self.card_width + self.card_height) / 2) * self.card_scale) * CARD_MARGIN_RATIO

    def get_player_text_x(self) -> float:
        """"""
        return self.get_card_x(1) - (self.card_width / 2) - self.card_margin

    def get_card_x(self, card_num: int) -> float:
        """"""
        return (self.window.width / 2) + (
            (self.card_width + (self.card_margin / 2)) * (card_num - 2)
        )

    def get_score_text_x(self, num_cards: int) -> float:
        """"""
        return self.get_card_x(num_cards) + (self.card_width / 2) + self.card_margin

    def get_player_y(self, player_num: int) -> float:
        """
        Get the player score center Y coordinate.
        """
        return (self.window.height * 0.75) - (
            (self.card_height + (self.card_margin * 0.75)) * (player_num - 1)
        )

    def get_total_score_button_x(self) -> float:
        """"""
        return self.window.width / 2

    def get_total_score_button_y(self) -> float:
        """"""
        return self.window.height * 0.175


class TotalScoreButton(arcade.gui.UIFlatButton):
    """"""

    def __init__(self, view: ResultView, *args, **kwargs):
        """"""
        self.view = view
        super().__init__("Total Score", *args, **kwargs)

    def on_click(self) -> None:
        """"""
        total_score_view = TotalScoreView(self.view.engine, self.view.my_player_num)
        total_score_view.setup()
        self.view.window.show_view(total_score_view)
