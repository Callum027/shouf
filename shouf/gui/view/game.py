# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

import enum
import itertools
import time

from typing import TYPE_CHECKING, Dict, Iterable, Mapping, Optional, Sequence, Tuple, Union

import arcade
import arcade.gui

from shouf.core.action.listener.cache import ActionListenerCache

from shouf.core.card import Card, Rank
from shouf.core.controller import Controller
from shouf.core.state import State, Phase

from shouf.core.exceptions import LegalityError

# from shouf.core.state import State

from shouf.gui.sprite.card import CardSprite, CardSpriteList, CARD_WIDTH, CARD_HEIGHT

from shouf.gui.view.result import ResultView

if TYPE_CHECKING:
    from shouf.core.engine import Engine
    from shouf.core.player.local import LocalPlayer


Point = Tuple[Union[int, float], Union[int, float]]
Size = Tuple[Union[int, float], Union[int, float]]

CardTableIndex = Tuple[str, Optional[int], Optional[int]]
MatTableIndex = Tuple[str, Optional[int]]


CARD_MARGIN_RATIO = 0.2
CARD_MAT_SIZE_RATIO = 0.1


class CardEffectType(enum.Enum):
    """"""

    SHOW_OWN_CARD = 0
    SHOW_OTHER_CARD = 1
    SWAP_PLAYERS_CARDS_UNKNOWN = 2
    SWAP_PLAYERS_CARDS_KNOWN = 3


class GameView(arcade.View, ActionListenerCache):
    """"""

    def __init__(self, player: LocalPlayer):
        """"""
        #
        self.my_player = player
        #
        self.ui_manager = arcade.gui.UIManager(attach_callbacks=False)
        #
        self.controller: Optional[Controller] = None
        #
        self.all_cards: CardSpriteList = CardSpriteList()
        self.deck: CardSpriteList = CardSpriteList()
        self.discard_pile: CardSpriteList = CardSpriteList()
        self.hand_card: Optional[CardSprite] = None
        self.player_piles: Dict[int, Dict[int, CardSprite]] = {}
        #
        self.deck_mat: Optional[arcade.Sprite] = None
        self.discard_pile_mat: Optional[arcade.Sprite] = None
        self.hand_card_mat: Optional[arcade.Sprite] = None
        self.player_pile_mats: Dict[int, arcade.Sprite] = None
        #
        self.held_card: Optional[Tuple[str, Optional[int], Optional[int]]] = None
        #
        self.card_effect_type: Optional[CardEffectType] = None
        self.cardeffect_swap_card_num: Optional[int] = None
        self.cardeffect_swap_other_player_num: Optional[int] = None
        self.cardeffect_swap_other_card_num: Optional[int] = None
        # Call the Arcade View initialisation code.
        arcade.View.__init__(self)
        ActionListenerCache.__init__(self)

    #
    #
    #

    def subscribe_to_engine(self, engine: Engine):
        """"""
        super().subscribe_to_engine(engine)
        self.controller = Controller(self.engine, self.my_player_num)

    #
    #
    #

    def setup(self) -> None:
        """"""
        #
        self.ui_manager.register_handlers()
        #
        # self.all_cards = CardSpriteList()
        # self.deck = CardSpriteList()
        # self.discard_pile = CardSpriteList()
        # self.hand_card = None
        # self.player_piles = {}

    def on_show_view(self):
        """"""
        #
        self.ui_manager.purge_ui_elements()
        # self.ui_manager.unregister_handlers()
        #
        self.endturn_button = EndTurnButton(
            self.window,
            self,
            center_x=92,
            center_y=self.window.height - 260,
            width=128,
            height=50,
        )
        self.ui_manager.add_ui_element(self.endturn_button)
        #
        self.callshouf_button = CallShoufButton(
            self.window,
            self,
            center_x=92,
            center_y=self.window.height - 326,
            width=144,
            height=50,
        )
        self.ui_manager.add_ui_element(self.callshouf_button)
        #
        self.ui_manager.register_handlers()
        # self.setup()
        # arcade.set_background_color(arcade.color.AMAZON)
        #
        self.deck_mat = self.create_card_mat(self.get_deck_location())
        self.discard_pile_mat = self.create_card_mat(self.get_discard_pile_location())
        self.hand_card_mat = self.create_card_mat(self.get_hand_card_location())
        self.player_pile_mats = {
            player_num: self.create_card_mat(self.get_player_pile_location(player_num), 2, 2)
            for player_num in self.engine.player_nums
        }
        self.all_mats = arcade.SpriteList()
        self.all_mats.extend(
            list(
                itertools.chain(
                    [self.deck_mat, self.discard_pile_mat, self.hand_card_mat],
                    self.player_pile_mats.values(),
                ),
            ),
        )

    def on_hide_view(self):
        """"""
        print("GameView.on_hide_view called")
        # self.ui_manager.purge_ui_elements()
        self.ui_manager.purge_ui_elements()
        self.ui_manager.unregister_handlers()

    #
    #
    #

    @property
    def my_player_num(self) -> int:
        return self.my_player.num

    @property
    def current_state(self) -> State:
        """"""
        return self.engine.current_state

    @property
    def started(self) -> bool:
        """"""
        return self.engine.current_state.started

    @property
    def current_turn(self) -> int:
        """"""
        return self.engine.current_state.current_turn

    @property
    def current_phase(self) -> Phase:
        """"""
        return self.engine.current_state.current_phase

    @property
    def current_player(self) -> int:
        """"""
        return self.engine.current_state.current_player

    #
    #
    #

    def on_new_game(self) -> None:
        """"""
        #
        self.all_cards = CardSpriteList()
        for card in self.engine.current_state.deck:
            self.all_cards.append(self.create_card_sprite(card))
        #
        self.deck = CardSpriteList()
        self.discard_pile = CardSpriteList()
        self.hand_card = None
        self.player_piles = {player_num: {} for player_num in self.engine.player_nums}

    def on_shuffle_deck(self, shuffled_deck: Sequence[Card]) -> None:
        """
        Deal a card to the player specified in the action object.
        """
        suitrank_cardsprite = {
            (card_sprite.card.suit, card_sprite.card.rank): card_sprite
            for card_sprite in self.all_cards
        }
        self.deck = CardSpriteList()
        for card in reversed(shuffled_deck):
            self.deck.append(
                self.move_card_sprite(suitrank_cardsprite[(card.suit, card.rank)], to="deck"),
            )

    def on_deal_card(self, player_num: int, card_num: int) -> None:
        """
        Deal a card to the player specified in the action object.
        """
        card = self.deck.pop()
        self.player_piles[player_num][card_num] = card
        self.move_card_sprite(card, to="player_pile", player_num=player_num, card_num=card_num)

    def on_finish_dealing_cards(self) -> None:
        """"""
        pass

    def on_show_front_two_cards(self) -> None:
        """"""
        for card_num in (2, 4):
            self.player_piles[self.my_player_num][card_num].set_faceup()

    def hide_player_cards(self) -> None:
        """"""
        for player_pile in self.player_piles.values():
            for card in player_pile.values():
                card.set_facedown()

    def on_start_game(self) -> None:
        """"""
        self.hide_player_cards()

    def on_draw_from_deck(self, player_num: int) -> None:
        """"""
        self.hand_card = self.deck.pop()
        self.move_card_sprite(self.hand_card, to="hand")
        if player_num == self.my_player_num:
            self.hand_card.set_faceup()
        else:
            self.hand_card.set_facedown()

    def on_draw_from_discard(self, player_num: int) -> None:
        """"""
        self.hand_card = self.discard_pile.pop()
        self.move_card_sprite(self.hand_card, to="hand")
        self.hand_card.set_faceup()

    def on_play_card(self, player_num: int) -> None:
        """"""
        hand_card = self.hand_card
        hand_card.set_faceup()
        self.hand_card = None
        self.discard_pile.append(hand_card)
        self.move_card_sprite(hand_card, to="discard_pile")

    def on_switch_cards(self, player_num: int, card_num: int) -> None:
        """"""
        #
        hand_card = self.hand_card
        facedown_card = self.player_piles[player_num][card_num]
        #
        hand_card.set_facedown()
        facedown_card.set_faceup()
        #
        self.hand_card = None
        self.player_piles[player_num][card_num] = hand_card
        self.discard_pile.append(facedown_card)
        #
        self.move_card_sprite(facedown_card, to="discard_pile")
        self.move_card_sprite(
            hand_card,
            to="player_pile",
            player_num=player_num,
            card_num=card_num,
        )

    def on_swap_players_cards(
        self,
        player_num: int,
        card_num: int,
        other_player_num: int,
        other_card_num: int,
        show_cards: bool,
    ) -> None:
        """"""
        #
        player_card = self.player_piles[player_num][card_num]
        other_card = self.player_piles[other_player_num][other_card_num]
        #
        player_card.set_facedown()
        other_card.set_facedown()
        #
        self.player_piles[player_num][card_num] = other_card
        self.player_piles[other_player_num][other_card_num] = player_card
        #
        self.move_card_sprite(
            player_card,
            to="player_pile",
            player_num=other_player_num,
            card_num=other_card_num,
        )
        self.move_card_sprite(
            other_card,
            to="player_pile",
            player_num=player_num,
            card_num=card_num,
        )

    def on_show_card(self, player_num: int, target_player_num: int, target_card_num: int) -> None:
        """"""
        pass

    def on_no_card_effect(self, player_num: int) -> None:
        """"""
        pass

    def on_stack_card(self, player_num: int, target_player_num: int, target_card_num: int) -> None:
        """"""
        target_card = self.player_piles[target_player_num][target_card_num]
        target_card.set_faceup()
        self.discard_pile.append(target_card)
        del self.player_piles[target_player_num][target_card_num]
        self.move_card_sprite(target_card, to="discard_pile")

    def on_move_card(
        self,
        player_num: int,
        card_num: int,
        target_player_num: int,
        target_card_num: int,
    ) -> None:
        """"""
        player_card = self.player_piles[player_num][card_num]
        self.player_piles[target_player_num][target_card_num] = player_card
        self.move_card_sprite(
            player_card,
            to="player_pile",
            player_num=target_player_num,
            card_num=target_card_num,
        )

    def on_call_shouf(self, player_num: int) -> None:
        """"""
        pass

    def on_end_turn(self, player_num: int) -> None:
        """"""
        if self.current_phase == Phase.END_GAME:
            result_view = ResultView(self.engine, self.my_player_num)
            result_view.setup()
            self.window.show_view(result_view)

    #
    #
    #

    def open_confirm_card_effect_form(self, card: Card) -> None:
        """"""
        open_confirm_card_effect_view = ConfirmCardEffectView(self, card)
        open_confirm_card_effect_view.setup()
        self.window.show_view(open_confirm_card_effect_view)

    def trigger_card_effect(self, card: Card) -> None:
        """"""
        if card.rank in (Rank.SEVEN, Rank.EIGHT):
            self.card_effect_type = CardEffectType.SHOW_OWN_CARD
        elif card.rank in (Rank.NINE, Rank.TEN):
            self.card_effect_type = CardEffectType.SHOW_OTHER_CARD
        elif card.rank in (Rank.JACK, Rank.QUEEN):
            self.card_effect_type = CardEffectType.SWAP_PLAYERS_CARDS_UNKNOWN
        elif card.suit.is_black() and card.rank == Rank.KING:
            self.card_effect_type = CardEffectType.SHOW_OWN_CARD
        else:
            raise ValueError(f"Card has no effect: {card}")

    def trigger_no_card_effect(self) -> None:
        """"""
        self.controller.no_card_effect()

    #
    #
    #

    def get_deck_location(self) -> Tuple[float, float]:
        return (
            ((self.window.width / 2) - self.card_width - (self.card_margin / 2)),
            (self.window.height / 2),
        )

    def get_hand_card_location(self) -> Tuple[float, float]:
        return ((self.window.width / 2), (self.window.height / 2))

    def get_discard_pile_location(self) -> Tuple[float, float]:
        return (
            ((self.window.width / 2) + self.card_width + (self.card_margin / 2)),
            (self.window.height / 2),
        )

    def get_player_pile_location(self, player_num: int) -> Tuple[float, float]:
        """"""
        width, height = self.window.get_size()
        return {
            1: (int(width / 2), int(height * 0.25)),
            2: (int(width / 2), int(height * 0.75)),
            3: (int(width * 0.25), int(height / 2)),
            4: (int(width * 0.75), int(height / 2)),
        }[player_num]

    def get_player_pile_card_location(self, player_num: int, card_num: int) -> Tuple[float, float]:
        pile = self.get_player_pile_location(player_num)
        return (
            (
                pile[0]
                - int(self.card_width / 2)
                - int(self.card_margin / 2)
                + self.card_width_offset(int((card_num - 1) / 2))
            ),
            (
                pile[1]
                + int(self.card_height / 2)
                + int(self.card_margin / 2)
                - self.card_height_offset((card_num - 1) % 2)
            ),
        )

    def move_card_sprite(
        self,
        card: CardSprite,
        to: str,
        player_num: Optional[int] = None,
        card_num: Optional[int] = None,
    ) -> CardSprite:
        """"""
        if to == "deck":
            card.center_x, card.center_y = self.get_deck_location()
        elif to == "hand":
            card.center_x, card.center_y = self.get_hand_card_location()
        elif to == "discard_pile":
            card.center_x, card.center_y = self.get_discard_pile_location()
        elif to == "player_pile":
            card.center_x, card.center_y = self.get_player_pile_card_location(player_num, card_num)
        else:
            raise ValueError(f"Invalid value for 'to': {to}")
        self.pull_card_to_top(card)
        return card

    def pull_card_to_top(self, card: CardSprite) -> None:
        """ Pull card to top of rendering order (last to render, looks on-top) """
        # Find the index of the card.
        index = self.all_cards.index(card)
        #
        if index == len(self.all_cards) - 1:
            return
        # Loop and pull all the other cards down towards the zero end.
        for i in range(index, len(self.all_cards) - 1):
            self.all_cards[i] = self.all_cards[i + 1]
        # Put this card at the right-side/top/size of list.
        self.all_cards[len(self.all_cards) - 1] = card

    def card_width_offset(self, num_cards: int) -> float:
        """"""
        return num_cards * (self.card_width + self.card_margin)

    def card_height_offset(self, num_cards: int) -> float:
        """"""
        return num_cards * (self.card_height + self.card_margin)

    #
    #
    #

    def on_draw(self) -> None:
        """"""
        #
        self.notify_actions()
        #
        arcade.start_render()
        #
        if self.started:
            arcade.draw_text(
                (
                    f"Turn: {self.current_turn}\n"
                    f"Current player: {self.current_player}\n"
                    f"Phase: {self.current_phase.name}\n"
                    f"Card effect: {self.card_effect_type}\n"
                    f"Stacked player: {self.current_state.stacked_player}\n"
                    f"Shouf player: {self.current_state.shouf_player}"
                ),
                32,
                self.window.height - 32,
                arcade.color.WHITE,
                font_size=28,
                anchor_x="left",
                anchor_y="top",
            )
        #
        self.deck_mat.draw()
        self.discard_pile_mat.draw()
        self.hand_card_mat.draw()
        for player_pile_mat in self.player_pile_mats.values():
            player_pile_mat.draw()
        #
        if self.current_phase >= Phase.DECK_SHUFFLED:
            self.all_cards.draw()
        # #
        # self.deck.draw()
        # self.discard_pile.draw()
        # #
        # if self.hand_card:
        #     self.hand_card.draw()
        # #
        # for pile in self.player_piles.values():
        #     for card in pile.values():
        #         card.draw()

    def on_resize(self, width, height):
        """"""
        #
        self.window.on_resize(width, height)
        #
        scale = self.card_scale
        #
        deck_location = self.get_deck_location()
        discard_pile_location = self.get_discard_pile_location()
        hand_card_location = self.get_hand_card_location()
        single_card_mat_size = self.get_card_mat_size()
        #
        self.resize_card_mat(self.deck_mat, deck_location, single_card_mat_size)
        self.resize_card_mat(self.discard_pile_mat, discard_pile_location, single_card_mat_size)
        self.resize_card_mat(self.hand_card_mat, hand_card_location, single_card_mat_size)
        #
        for card in self.deck:
            card.scale = scale
            card.center_x, card.center_y = deck_location
        #
        for card in self.discard_pile:
            card.scale = scale
            card.center_x, card.center_y = discard_pile_location
        #
        if self.hand_card:
            self.hand_card.scale = scale
            self.hand_card.center_x, self.hand_card.center_y = hand_card_location
        #
        for player_num, pile in self.player_piles.items():
            player_pile_location = self.get_player_pile_location(player_num)
            self.resize_card_mat(
                self.player_pile_mats[player_num],
                player_pile_location,
                self.get_card_mat_size_for_player_pile(pile),
            )
            for card_num, card in pile.items():
                card.scale = scale
                card.center_x, card.center_y = self.get_player_pile_card_location(
                    player_num,
                    card_num,
                )

    def resize_card_mat(self, mat: arcade.Sprite, location: Point, size: Size) -> None:
        """"""
        mat.center_x, mat.center_y = location
        mat.width, mat.height = size

    #
    #
    #

    def get_clicked_card(
        self,
        x: float,
        y: float,
        ignore_cards: Iterable[CardSprite] = None,
    ) -> Optional[CardTableIndex]:
        """"""
        ignore_cards_set = set(ignore_cards) if ignore_cards else set()
        clicked_cards = [
            card
            for card in arcade.get_sprites_at_point((x, y), self.all_cards.sprite_list)
            if card not in ignore_cards_set
        ]
        if self.deck and self.deck[-1] in clicked_cards:
            return ("deck", None, None)
        if self.discard_pile and self.discard_pile[-1] in clicked_cards:
            return ("discard_pile", None, None)
        if self.hand_card and self.hand_card in clicked_cards:
            return ("hand", None, None)
        for player_num, pile in self.player_piles.items():
            for card_num, card in pile.items():
                if card in clicked_cards:
                    return ("player_pile", player_num, card_num)
        return None

    def get_clicked_mat(self, x: float, y: float) -> Optional[MatTableIndex]:
        """"""
        clicked_mats = arcade.get_sprites_at_point((x, y), self.all_mats)
        if self.deck_mat in clicked_mats:
            return ("deck", None)
        elif self.discard_pile_mat in clicked_mats:
            return ("discard_pile", None)
        elif self.hand_card_mat in clicked_mats:
            return ("hand", None)
        for player_num, pile_mat in self.player_pile_mats.items():
            if pile_mat in clicked_mats:
                return ("player_pile", player_num)
        return None

    def get_card_from_table_index(self, table_index: CardTableIndex) -> CardSprite:
        """"""
        if table_index[0] == "deck":
            return self.deck[-1]
        elif table_index[0] == "discard_pile":
            return self.discard_pile[-1]
        elif table_index[0] == "hand":
            return self.hand_card
        elif table_index[0] == "player_pile":
            return self.player_piles[table_index[1]][table_index[2]]
        else:
            raise ValueError(f"Invalid table index: {table_index}")

    def on_mouse_press(self, x: float, y: float, button: int, key_modifiers: int) -> None:
        """"""
        #
        if (
            self.current_phase < Phase.NEW_TURN
            or self.current_phase >= Phase.SHOUF_CALLED
            or self.my_player_num in self.current_state.ended_turn_players
        ):
            return
        #
        held_card = self.get_clicked_card(x, y)
        #
        if not held_card:
            return
        #
        if self.current_phase == Phase.CARD_EFFECT_USED:
            if held_card[0] != "player_pile":
                return
        #
        elif self.current_phase == Phase.CARD_STACKED:
            if self.my_player_num != self.current_state.stacked_player:
                return
            if held_card[0] != "player_pile":
                return
        #
        else:
            if self.current_player != self.my_player_num:
                return
            #
            if self.current_phase == Phase.NEW_TURN:
                if held_card[0] not in ("deck", "discard_pile"):
                    return
            #
            elif self.current_phase == Phase.CARD_DRAWN:
                if held_card[0] != "hand":
                    return
            #
            elif self.current_phase == Phase.CARD_USED:
                if self.card_effect_type == CardEffectType.SHOW_OWN_CARD:
                    if held_card[0] != "player_pile" or held_card[1] != self.my_player_num:
                        return
                elif self.card_effect_type == CardEffectType.SHOW_OTHER_CARD:
                    if held_card[0] != "player_pile" or held_card[1] == self.my_player_num:
                        return
                elif self.card_effect_type in (
                    CardEffectType.SWAP_PLAYERS_CARDS_KNOWN,
                    CardEffectType.SWAP_PLAYERS_CARDS_UNKNOWN,
                ):
                    if held_card[0] != "player_pile":
                        return
        #
        self.held_card = held_card
        #
        self.pull_card_to_top(self.get_card_from_table_index(self.held_card))

    def on_mouse_release(self, x: float, y: float, button: int, key_modifiers: int) -> None:
        """"""
        try:
            # TODO: Automate this and do it somewhere else.
            if not self.started:
                if self.current_phase == Phase.NEW_STATE:
                    self.engine.new_game()
                elif self.current_phase == Phase.NEW_GAME:
                    self.engine.shuffle_deck()
                    self.engine.deal_cards()
                elif self.current_phase == Phase.CARDS_DEALED:
                    self.engine.show_front_two_cards()
                else:
                    self.engine.start_game()
                return
            #
            if not self.held_card:
                return
            #
            moved_card = False
            if self.current_phase == Phase.NEW_TURN:
                moved_card = self.on_mouse_release_new_turn(x, y)
            elif self.current_phase == Phase.CARD_DRAWN:
                moved_card = self.on_mouse_release_deck_drawn(x, y)
            elif self.current_phase == Phase.CARD_USED:
                moved_card = self.on_mouse_release_card_used(x, y)
            elif self.current_phase in (Phase.CARD_EFFECT_USED, Phase.CARD_STACKED):
                moved_card = self.on_mouse_release_card_stacked(x, y)
            #
            if not moved_card:
                self.move_card_sprite(
                    self.get_card_from_table_index(self.held_card),
                    to=self.held_card[0],
                    player_num=self.held_card[1],
                    card_num=self.held_card[2],
                )
        #
        except LegalityError:
            self.move_card_sprite(
                self.get_card_from_table_index(self.held_card),
                to=self.held_card[0],
                player_num=self.held_card[1],
                card_num=self.held_card[2],
            )
            raise
        #
        finally:
            self.held_card = None

    def on_mouse_release_new_turn(self, x: float, y: float) -> bool:
        """"""
        #
        clicked_mat = self.get_clicked_mat(x, y)
        #
        if not clicked_mat:
            return False
        #
        if clicked_mat[0] != "hand":
            return False
        #
        if self.held_card[0] == "deck":
            self.controller.draw_from_deck()
            return True
        elif self.held_card[0] == "discard_pile":
            self.controller.draw_from_discard()
            return True
        #
        return False

    def on_mouse_release_deck_drawn(self, x: float, y: float) -> bool:
        """"""
        #
        clicked_mat = self.get_clicked_mat(x, y)
        #
        if not clicked_mat:
            return False
        #
        if self.held_card[0] != "hand":
            return False
        #
        if clicked_mat[0] == "player_pile" and clicked_mat[1] == self.my_player_num:
            clicked_card = self.get_clicked_card(x, y, ignore_cards=[self.hand_card])
            self.controller.switch_cards(clicked_card[2])
            return True
        #
        elif self.current_state.drew_from_deck and clicked_mat[0] == "discard_pile":
            hand_card = self.hand_card
            self.controller.play_card()
            if hand_card.card.has_effect():
                self.open_confirm_card_effect_form(hand_card.card)
            return True
        #
        return False

    def on_mouse_release_card_used(self, x: float, y: float) -> bool:
        """"""
        #
        clicked_mat = self.get_clicked_mat(x, y)
        #
        if not clicked_mat:
            return False
        #
        if self.card_effect_type in (CardEffectType.SHOW_OWN_CARD, CardEffectType.SHOW_OTHER_CARD):
            #
            clicked_card = self.get_clicked_card(x, y)
            if clicked_card != self.held_card:
                return False
            #
            if self.held_card[0] != "player_pile":
                return False
            #
            if (
                self.card_effect_type == CardEffectType.SHOW_OWN_CARD
                and self.held_card[1] == self.my_player_num
            ):
                card = self.player_piles[self.held_card[1]][self.held_card[2]]
                if card.is_facedown:
                    card.set_faceup()
                else:
                    card.set_facedown()
                    self.controller.show_own_card(card_num=self.held_card[2])
                    self.card_effect_type = None
                return True
            #
            elif (
                self.card_effect_type == CardEffectType.SHOW_OTHER_CARD
                and self.held_card[1] != self.my_player_num
            ):
                card = self.player_piles[self.held_card[1]][self.held_card[2]]
                if card.is_facedown:
                    card.set_faceup()
                else:
                    card.set_facedown()
                    self.controller.show_other_player_card(
                        target_player_num=self.held_card[1],
                        target_card_num=self.held_card[2],
                    )
                    self.card_effect_type = None
                return True
        #
        elif self.card_effect_type in (
            CardEffectType.SWAP_PLAYERS_CARDS_KNOWN,
            CardEffectType.SWAP_PLAYERS_CARDS_UNKNOWN,
        ):
            #
            clicked_card = self.get_clicked_card(x, y)
            if clicked_card != self.held_card:
                return False
            #
            if self.held_card[0] != "player_pile":
                return False
            #
            if self.held_card[1] == self.my_player_num:
                if self.cardeffect_swap_card_num:
                    return False
                self.cardeffect_swap_card_num = self.held_card[2]
            #
            else:
                if self.cardeffect_swap_other_player_num:
                    return False
                self.cardeffect_swap_other_player_num = self.held_card[1]
                self.cardeffect_swap_other_card_num = self.held_card[2]
            #
            if self.card_effect_type == CardEffectType.SWAP_PLAYERS_CARDS_KNOWN:
                card.set_faceup()
            # TODO: Move this somewhere else.
            if self.card_effect_type == CardEffectType.SWAP_PLAYERS_CARDS_KNOWN:
                time.sleep(1)
            if self.cardeffect_swap_card_num and self.cardeffect_swap_other_player_num:
                #
                player_num = self.my_player_num
                card_num = self.cardeffect_swap_card_num
                other_player_num = self.cardeffect_swap_other_player_num
                other_card_num = self.cardeffect_swap_other_card_num
                #
                self.player_piles[player_num][card_num].set_facedown()
                self.player_piles[other_player_num][other_card_num].set_facedown()
                self.controller.swap_players_cards(
                    card_num=card_num,
                    other_player_num=other_player_num,
                    other_card_num=other_card_num,
                    show_cards=self.card_effect_type == CardEffectType.SWAP_PLAYERS_CARDS_KNOWN,
                )
                #
                self.cardeffect_swap_card_num = None
                self.cardeffect_swap_other_player_num = None
                self.cardeffect_swap_other_card_num = None
                #
                self.card_effect_type = None
                #
                return True
            #
            return False
        #
        return False

    def on_mouse_release_card_stacked(self, x: float, y: float) -> bool:
        """"""
        #
        clicked_mat = self.get_clicked_mat(x, y)
        #
        if not clicked_mat:
            return False
        #
        if (
            self.current_phase == Phase.CARD_STACKED
            and self.my_player_num != self.current_state.stacked_player
        ):
            return False
        #
        if self.held_card[0] != "player_pile":
            return False
        #
        if clicked_mat[0] == "discard_pile":
            #
            stacked_card = self.player_piles[self.held_card[1]][self.held_card[2]]
            discard_card = self.discard_pile[-1]
            #
            if stacked_card.card.rank != discard_card.card.rank:
                return False
            #
            self.controller.stack_card(
                target_player_num=self.held_card[1],
                target_card_num=self.held_card[2],
            )
        #
        elif clicked_mat[0] == "player_pile":
            #
            if self.held_card[1] != self.my_player_num:
                return False
            #
            if clicked_mat[1] == self.my_player_num:
                return False
            #
            for target_player_num, target_card_num in self.current_state.stacked_previous_targets:
                if target_player_num == clicked_mat[1]:
                    break
            else:
                return False
            #
            self.controller.move_card(
                card_num=self.held_card[2],
                target_player_num=target_player_num,
                target_card_num=target_card_num,
            )

        return True

    def on_mouse_motion(self, x: float, y: float, dx: float, dy: float):
        """"""
        #
        card: Optional[CardSprite] = None
        if self.held_card:
            if self.held_card[0] == "deck":
                card = self.deck[-1]
            elif self.held_card[0] == "discard_pile":
                card = self.discard_pile[-1]
            elif self.held_card[0] == "hand":
                card = self.hand_card
            elif self.held_card[0] == "player_pile":
                card = self.player_piles[self.held_card[1]][self.held_card[2]]
        if card:
            card.center_x += dx
            card.center_y += dy

    #
    #
    #

    @property
    def sprite_scale(self) -> float:
        """"""
        width, height = self.window.get_size()
        return ((width / 1920) + (height / 1080)) / 2

    @property
    def card_scale(self) -> float:
        """"""
        return 0.7 * self.sprite_scale

    @property
    def card_width(self) -> float:
        """"""
        return CARD_WIDTH * self.card_scale

    @property
    def card_height(self) -> float:
        """"""
        return CARD_HEIGHT * self.card_scale

    @property
    def card_mat_width(self) -> int:
        """"""
        return round(self.card_width + (self.card_width * CARD_MAT_SIZE_RATIO))

    @property
    def card_mat_height(self) -> int:
        """"""
        return round(self.card_height + (self.card_height * CARD_MAT_SIZE_RATIO))

    @property
    def card_margin(self) -> float:
        """"""
        return self.card_width * CARD_MARGIN_RATIO

    def get_card_mat_size(self, num_cards_x: int = 1, num_cards_y: int = 1) -> Tuple[int, int]:
        """"""
        return (self.card_mat_width * num_cards_x, self.card_mat_height * num_cards_y)

    def get_card_mat_size_for_player_pile(
        self,
        player_pile: Mapping[int, CardSprite],
    ) -> Tuple[float, float]:
        """"""
        highest_card_num = sorted(player_pile.keys())[-1] if player_pile else 0
        return self.get_card_mat_size(max(2, int(highest_card_num / 2)), 2)

    def create_card_sprite(self, card: Card, **kwargs) -> CardSprite:
        """"""
        return CardSprite(
            card,
            scale=self.card_scale,
            **kwargs,
        )

    def create_card_mat(
        self,
        location: Point,
        num_cards_x: int = 1,
        num_cards_y: int = 1,
    ) -> arcade.SpriteSolidColor:
        """"""
        width, height = self.get_card_mat_size(num_cards_x, num_cards_y)
        mat = arcade.SpriteSolidColor(
            width,
            height,
            arcade.csscolor.DARK_OLIVE_GREEN,
        )
        mat.center_x, mat.center_y = location
        return mat


class ConfirmCardEffectView(arcade.View):
    """"""

    def __init__(self, game_view: GameView, card: Card):
        """"""
        self.game_view = game_view
        self.card = card
        self.ui_manager = arcade.gui.UIManager(attach_callbacks=False)
        super().__init__()

    def setup(self):
        """"""
        self.ui_manager.register_handlers()

    def on_show_view(self):
        """ Called once when view is activated. """
        #
        self.ui_manager.purge_ui_elements()
        #
        width, height = self.window.get_size()
        #
        self.yes_button = CardEffectButton(
            self.window,
            self,
            "Yes",
            center_x=width * 0.4,
            center_y=height / 2,
            width=128,
            height=50,
        )
        self.no_button = CardEffectButton(
            self.window,
            self,
            "No",
            center_x=width * 0.6,
            center_y=height / 2,
            width=128,
            height=50,
        )
        #
        self.ui_manager.add_ui_element(self.yes_button)
        self.ui_manager.add_ui_element(self.no_button)
        # self.setup()
        # arcade.set_background_color(arcade.color.AMAZON)

    def on_hide_view(self):
        """"""
        self.ui_manager.purge_ui_elements()
        self.ui_manager.unregister_handlers()

    def on_draw(self):
        """"""
        arcade.start_render()
        arcade.draw_text(
            "You played an effect card from the deck.\nUse card effect?",
            self.window.width / 2,
            self.window.height * 0.75,
            arcade.color.WHITE,
            font_size=64,
            align="center",
            anchor_x="center",
            anchor_y="center",
        )


class CardEffectButton(arcade.gui.UIFlatButton):
    """"""

    def __init__(self, window: arcade.Window, view: ConfirmCardEffectView, *args, **kwargs):
        """"""
        self.window = window
        self.view = view
        super().__init__(*args, **kwargs)

    def on_click(self) -> None:
        """"""
        if self.text == "Yes":
            self.view.game_view.trigger_card_effect(self.view.card)
        elif self.text == "No":
            self.view.game_view.trigger_no_card_effect()
        else:
            raise ValueError(f"Invalid text: {self.text}")
        self.view.setup()
        self.window.show_view(self.view.game_view)


class EndTurnButton(arcade.gui.UIFlatButton):
    """"""

    def __init__(self, window: arcade.Window, view: GameView, *args, **kwargs):
        """"""
        self.window = window
        self.view = view
        super().__init__("End Turn", *args, **kwargs)

    def on_click(self) -> None:
        """"""
        if not self.view.current_phase >= Phase.CARD_EFFECT_USED:
            return
        if self.view.my_player_num in self.view.current_state.ended_turn_players:
            return
        self.view.controller.end_turn()


class CallShoufButton(arcade.gui.UIFlatButton):
    """"""

    def __init__(self, window: arcade.Window, view: GameView, *args, **kwargs):
        """"""
        self.window = window
        self.view = view
        super().__init__("Call Shouf", *args, **kwargs)

    def on_click(self) -> None:
        """"""
        if self.view.my_player_num != self.view.current_player:
            return
        #
        if self.view.current_phase != Phase.NEW_TURN:
            return
        #
        self.view.controller.call_shouf()
