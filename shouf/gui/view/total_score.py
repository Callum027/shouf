# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

from typing import TYPE_CHECKING, Dict

import arcade
import arcade.gui

from shouf.gui.sprite.card import CardSprite, CARD_WIDTH, CARD_HEIGHT

from shouf.gui.view import start as start_view
from shouf.gui.view import load_game as load_game_view

if TYPE_CHECKING:
    from shouf.core.card import Card
    from shouf.core.engine import Engine
    from shouf.core.player.base import Player


CARD_MARGIN_RATIO = 0.2
CARD_MAT_SIZE_RATIO = 0.1


class TotalScoreView(arcade.View):
    """"""

    def __init__(self, engine: Engine, my_player_num: int):
        """"""
        #
        self.engine = engine
        self.my_player_num = my_player_num
        #
        self.state = self.engine.current_state
        #
        self.player_piles: Dict[int, Dict[int, CardSprite]] = {}
        #
        self.ui_manager = arcade.gui.UIManager(attach_callbacks=False)
        # Call the Arcade View initialisation code.
        super().__init__()

    @property
    def players(self) -> Dict[int, Player]:
        """"""
        return self.engine.players

    def setup(self) -> None:
        """"""
        #
        self.ui_manager.register_handlers()

    def on_show_view(self):
        """"""
        #
        # self.ui_manager.unregister_handlers()
        self.ui_manager.purge_ui_elements()
        #
        self.main_menu_button = MainMenuButton(
            self,
            center_x=self.get_main_menu_button_x(),
            center_y=self.get_button_y(),
        )
        self.next_game_button = NextGameButton(
            self,
            center_x=self.get_next_game_button_x(),
            center_y=self.get_button_y(),
        )
        self.ui_manager.add_ui_element(self.main_menu_button)
        self.ui_manager.add_ui_element(self.next_game_button)
        #
        if not self.player_piles:
            for player_num, pile in self.state.player_piles.items():
                self.player_piles[player_num] = {}
                for card_num, card in enumerate(pile.values(), 1):
                    self.player_piles[player_num][card_num] = self.create_card_sprite(
                        player_num,
                        card_num,
                        card,
                    )
        #
        # self.ui_manager.register_handlers()
        # self.setup()
        # arcade.set_background_color(arcade.color.AMAZON)

    def on_hide_view(self):
        """"""
        self.ui_manager.purge_ui_elements()
        self.ui_manager.unregister_handlers()
        # self.ui_manager.purge_ui_elements()

    def on_draw(self) -> None:
        """"""
        #
        arcade.start_render()
        #
        player_scores = self.state.player_scores
        winning_players = self.state.winning_players
        #
        if len(winning_players) == 1:
            players_win_text = f"{self.players[winning_players[0]].name} wins!"
        elif winning_players:
            players_win_text = (
                ", ".join(self.players[pn].name for pn in winning_players[:-1])
                + f" and {self.players[winning_players[-1]].name} win!"
            )
        else:
            raise ValueError(f"Invalid state for winning_players: {winning_players}")
        #
        for player_num, pile in self.player_piles.items():
            #
            arcade.draw_text(
                f"{self.players[player_num].name}:",
                start_x=self.get_player_text_x(),
                start_y=self.get_player_y(player_num),
                color=arcade.color.WHITE,
                font_size=28,
                anchor_x="right",
                anchor_y="center",
            )
            #
            for card in pile.values():
                card.draw()
            #
            arcade.draw_text(
                f"= {player_scores[player_num]}",
                start_x=self.get_score_text_x(len(pile)),
                start_y=self.get_player_y(player_num),
                color=arcade.color.WHITE,
                font_size=28,
                anchor_x="left",
                anchor_y="center",
            )
        #
        arcade.draw_text(
            players_win_text,
            start_x=self.window.width / 2,
            start_y=self.get_player_y(len(player_scores) + 1),
            color=arcade.color.WHITE,
            font_size=28,
            anchor_x="center",
            anchor_y="center",
        )

    def on_resize(self, width, height):
        """"""
        #
        self.window.on_resize(width, height)
        #
        for player_num, pile in self.player_piles.items():
            for card_num, card in pile.items():
                card.scale = self.card_scale
                card.center_x = self.get_card_x(card_num)
                card.center_y = self.get_player_y(player_num)
        #
        self.main_menu_button.center_x = self.get_main_menu_button_x()
        self.main_menu_button.center_y = self.get_button_y()
        self.next_game_button.center_x = self.get_next_game_button_x()
        self.next_game_button.center_y = self.get_button_y()

    def create_card_sprite(self, player_num: int, card_num: int, card: Card) -> CardSprite:
        """"""
        return CardSprite(
            card,
            faceup=True,
            scale=self.card_scale,
            center_x=self.get_card_x(card_num),
            center_y=self.get_player_y(player_num),
        )

    #
    #
    #

    @property
    def scale(self) -> float:
        """"""
        width, height = self.window.get_size()
        return ((width / 1920) + (height / 1080)) / 2

    @property
    def card_scale(self) -> float:
        """"""
        return 0.5 * self.scale

    @property
    def card_width(self) -> float:
        """"""
        return CARD_WIDTH * self.card_scale

    @property
    def card_height(self) -> float:
        """"""
        return CARD_HEIGHT * self.card_scale

    @property
    def card_margin(self) -> float:
        """"""
        return (((self.card_width + self.card_height) / 2) * self.card_scale) * CARD_MARGIN_RATIO

    def get_player_text_x(self) -> float:
        """"""
        return self.get_card_x(1) - (self.card_width / 2) - self.card_margin

    def get_card_x(self, card_num: int) -> float:
        """"""
        return (self.window.width / 2) + (
            (self.card_width + (self.card_margin / 2)) * (card_num - 2)
        )

    def get_score_text_x(self, num_cards: int) -> float:
        """"""
        return self.get_card_x(num_cards) + (self.card_width / 2) + self.card_margin

    def get_player_y(self, player_num: int) -> float:
        """
        Get the player score center Y coordinate.
        """
        return (self.window.height * 0.75) - (
            (self.card_height + (self.card_margin * 0.75)) * (player_num - 1)
        )

    def get_main_menu_button_x(self) -> float:
        """"""
        return self.window.width * 0.4

    def get_next_game_button_x(self) -> float:
        """"""
        return self.window.width * 0.6

    def get_button_y(self) -> float:
        """"""
        return self.window.height * 0.175


class MainMenuButton(arcade.gui.UIFlatButton):
    """"""

    def __init__(self, view: TotalScoreView, *args, **kwargs):
        """"""
        self.view = view
        super().__init__("Main Menu", *args, **kwargs)

    def on_click(self) -> None:
        """"""
        new_view = start_view.StartView()
        new_view.setup()
        self.view.window.show_view(new_view)


class NextGameButton(arcade.gui.UIFlatButton):
    """"""

    def __init__(self, view: TotalScoreView, *args, **kwargs):
        """"""
        self.view = view
        super().__init__("Next Game", *args, **kwargs)

    def on_click(self) -> None:
        """"""
        new_view = load_game_view.LoadGameView(self.view.engine, self.view.my_player_num)
        self.view.window.show_view(new_view)
