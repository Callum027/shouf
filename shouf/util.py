# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

from typing import List, Optional, Union


Seed = Optional[Union[int, float, str, bytes, bytearray]]


def get_player_nums(num_players: int) -> List[int]:
    """"""

    return list(range(1, num_players + 1))


def get_card_nums(num_cards: int) -> List[int]:
    """"""

    return list(range(1, num_cards + 1))
