# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

from typing import TYPE_CHECKING, Dict, List, Set, Optional, Tuple

from shouf.computer.base import Computer

from shouf.core.card import Card, Rank
from shouf.core.state import Phase
from shouf.core.exceptions import LegalityError

if TYPE_CHECKING:
    from shouf.core.player.computer import ComputerPlayer


class HardComputer(Computer):
    """"""

    def __init__(self, player: ComputerPlayer):
        """"""
        super().__init__(player=player, thinking_time=(1.0, 1.75))

    def handle_draw_card(self) -> None:
        """"""
        # Call Shouf if we know all our cards, our combined score is 2 or less,
        # and no other players have called Shouf already.
        if (
            not self.current_state.shouf_player
            and not self.get_unknown_card_num(self.my_player_num)
            and self.player_scores[self.my_player_num] <= 2
        ):
            self.controller.call_shouf()
            return
        # If there is no cards in the discard pile,
        # we have no choice but to draw from the deck.
        if not self.discard_card:
            self.controller.draw_from_deck()
            return
        # If the top card on the discard pile is worth negative points (e.g. a Red King),
        # take it from the discard pile, as having it actually lowers the score of our pile.
        if self.discard_card.points < 0:
            self.controller.draw_from_discard()
            return
        # If we know we have a card of the same rank as the top card on the discard pile,
        # draw it, so we can use it to stack our cards.
        for card in self.player_piles[self.my_player_num].values():
            if card:
                #
                if (
                    self.discard_card.suit.is_black()
                    and self.discard_card.rank == Rank.KING
                    and card.suit.is_black()
                    and card.rank == Rank.KING
                ):
                    self.controller.draw_from_discard()
                    return
                #
                if card.rank == self.discard_card.rank:
                    self.controller.draw_from_discard()
                    return
        # Consider drawing from the discard pile if the top card is low value enough
        # to not have a card effect.
        if not self.discard_card.has_effect():
            # If the top card on the discard pile is worth less than any of the
            # cards in our pile we actually know the value of, then draw it.
            for card in self.player_piles[self.my_player_num].values():
                if card and card.points > self.discard_card.points:
                    self.controller.draw_from_discard()
                    return
        # When there is no strategic reason to draw from the discard pile,
        # just draw from the deck.
        self.controller.draw_from_deck()

    def handle_play_card(self) -> None:
        """"""
        # Check if we drew from the deck. If so, we can not only switch cards,
        # but play cards to the discard pile (and use their associated card effects.)
        can_play_cards = self.current_state.drew_from_deck
        # If the card we drew has a negative value, switch one of our face-down cards for it.
        if self.hand_card.points < 0:
            self.controller.switch_cards(self.get_most_replaceable_card_num())
            return
        # If at least one of our known facedown cards can be stacked with the drawn card,
        # play the card directly so that can happen.
        if can_play_cards:
            for card in self.player_piles[self.my_player_num].values():
                if card and card.rank == self.hand_card.rank:
                    self.controller.play_card()
                    return
        # If there are any groups of cards in our hand that can be stacked,
        # swap the hand card with one of those.
        rank_groups: Dict[Rank, List[int]] = {}
        for card_num, card in self.player_piles[self.my_player_num].items():
            if card:
                # Do not count cards with negative value as stackable,
                # since we want to keep them.
                if card.points < 0:
                    continue
                if card.rank not in rank_groups:
                    rank_groups[card.rank] = []
                rank_groups[card.rank].append(card_num)
        for rank_group in rank_groups.values():
            if len(rank_group) >= 2:
                self.controller.switch_cards(rank_group[0])
                return
        # If the card has a card effect, play it.
        if can_play_cards and self.hand_card.has_effect():
            self.controller.play_card()
            return
        # If we know what all our facedown cards are, and the card we drew
        # has a higher value than any other card in our hand, simply play it.
        if can_play_cards:
            for card in self.player_piles[self.my_player_num].values():
                if not card or card.points > self.hand_card.points:
                    break
            else:
                self.controller.play_card()
                return
        # Base case, when no other reasonable action can be done:
        # Replace the highest value card with the card we drew.
        self.controller.switch_cards(self.get_most_replaceable_card_num())

    def get_most_replaceable_card_num(self) -> int:
        """
        Determine the card in our face-down pile that should be replaced next.
        """
        # Get the highest value KNOWN card number, and figure out whether or not
        # there are unknown cards.
        highest_known_card_num: int = 0
        highest_known_card: Optional[Card] = None
        have_unknown_cards = False
        for card_num, card in self.player_piles[self.my_player_num].items():
            if card:
                if not highest_known_card or card.points > highest_known_card.points:
                    highest_known_card_num = card_num
                    highest_known_card = card
            else:
                have_unknown_cards = True
        # If we didn't find any unknown cards, return the highest value card we found.
        if not have_unknown_cards:
            return highest_known_card_num
        # If the highest known card has a value high enough to have a card effect,
        # use that as the next replaceable card, rather than one of our unknown cards.
        # This is because our unknown cards have a 50/50 chance of being a low value card,
        # so we need to be careful when replacing them.
        if highest_known_card and highest_known_card.has_effect():
            return highest_known_card_num
        # If we get to this point, we want to replace one of our unknown face-down cards.
        # Pick one randomly and return it.
        return self.get_unknown_card_num(self.my_player_num)

    def get_unknown_card_num(self, player_num: int) -> Optional[int]:
        """
        Return a random unknown card number from a specific player.
        If that player has no unknown cards, return None.
        """
        unknown_card_nums = [
            card_num for card_num, card in self.player_piles[player_num].items() if not card
        ]
        if not unknown_card_nums:
            return None
        return self.random.choice(unknown_card_nums)

    def get_unknown_player_and_card_num(self, include_me: bool = False) -> Tuple[int, int]:
        """
        Get a random unknown card from a random other player as a (player, card) number coordinate.
        Set include_me to True to include the computer player itself as a player.
        If no unknown card could be found, return (0, 0).
        """
        player_nums = [
            pn
            for pn in self.player_piles.keys()
            if (include_me or pn != self.my_player_num) and pn != self.current_state.shouf_player
        ]
        self.random.shuffle(player_nums)
        for player_num in player_nums:
            card_nums = [
                card_num for card_num, card in self.player_piles[player_num].items() if not card
            ]
            if not card_nums:
                continue
            return (player_num, self.random.choice(card_nums))
        return (0, 0)

    def handle_card_effect(self) -> None:
        """"""
        effect_card = self.discard_card
        if effect_card.rank in (Rank.SEVEN, Rank.EIGHT):
            self.handle_show_own_card()
        elif effect_card.rank in (Rank.NINE, Rank.TEN):
            self.handle_show_other_card()
        elif effect_card.rank in (Rank.JACK, Rank.QUEEN):
            self.handle_swap_players_cards(show_cards=False)
        elif effect_card.suit.is_black() and effect_card.rank == Rank.KING:
            self.handle_swap_players_cards(show_cards=True)
        else:
            raise ValueError(f"Card does not have card effect: {self.hand_card}")

    def handle_show_own_card(self) -> None:
        """"""
        #
        for card_num, card in self.player_piles[self.my_player_num].items():
            if not card:
                self.controller.show_own_card(card_num)
                return
        #
        self.controller.no_card_effect()

    def handle_show_other_card(self) -> None:
        """"""
        #
        players_with_unknown_cards: Set[int] = set()
        for player_num, cards in self.player_piles.items():
            if player_num == self.current_state.shouf_player:
                continue
            for card in cards.values():
                if not card:
                    players_with_unknown_cards.add(player_num)
        #
        if not players_with_unknown_cards:
            self.controller.no_card_effect()
            return
        #
        player_score: Dict[int, int] = {}
        for player_num in players_with_unknown_cards:
            player_score[player_num] = sum(
                (card.points for card in self.player_piles[player_num].values() if card),
            )
        #
        lowest_score = 0
        lowest_score_player_num = 0
        for player_num, score in player_score.items():
            if not lowest_score_player_num or score < lowest_score:
                lowest_score = score
                lowest_score_player_num = player_num
        #
        self.controller.show_other_player_card(
            lowest_score_player_num,
            self.get_unknown_card_num(lowest_score_player_num),
        )

    def handle_swap_players_cards(self, show_cards: bool) -> None:
        """"""
        # Get the card we want to replace the most in our hand.
        # This can be either a known card or an unknown card.
        card_num = self.get_most_replaceable_card_num()
        card = self.player_piles[self.my_player_num][card_num]
        # If the absolute worst card we have is a card of negative value (e.g. a Red King),
        # do not swap cards at all, because we clearly have a winning hand.
        if card and card.points < 0:
            self.controller.no_card_effect()
            return
        # Try to get the most valuable card we are aware of in another player's pile.
        other_player_num, other_card_num = self.get_best_other_card_num()
        other_card = (
            self.player_piles[other_player_num][other_card_num] if other_player_num else None
        )
        # If we have a result for the most valuable card in another player's hand,
        # try to compare that to the card we wish to replace.
        if other_card:
            # If the other player's card is worth less points than ours, and is low enough
            # that it does not have a card effect, swap their card with ours.
            if card and card.points > other_card.points and not other_card.has_effect():
                self.controller.swap_players_cards(
                    card_num,
                    other_player_num,
                    other_card_num,
                    show_cards=show_cards,
                )
                return
            # If we want to replace an unknown card, and the best card we are aware of
            # in another player's pile has a value lower than 4, replace our unknown card
            # with their valuable one.
            if not card and other_card.points < 4:
                self.controller.swap_players_cards(
                    card_num,
                    other_player_num,
                    other_card_num,
                    show_cards=show_cards,
                )
                return
        # If the above conditions have not been met, it is determined that it is either
        # impossible or not worth replacing our card with other players' cards we know about,
        # so randomly select an unknown card from another player.
        other_player_num, other_card_num = self.get_unknown_player_and_card_num()
        if other_card_num:
            # If we are allowed to look at the cards we are swapping to confirm beforehand,
            # add the card to our memory and run checks.
            if show_cards:
                other_card = self.current_state.player_piles[other_player_num][other_card_num]
                self.player_piles[other_player_num][other_card_num] = other_card
                # If the other card is worth less than ours and is low enough to not have a
                # card effect, swap them.
                if card and card.points > other_card.points and not other_card.has_effect():
                    self.controller.swap_players_cards(
                        card_num,
                        other_player_num,
                        other_card_num,
                        show_cards=show_cards,
                    )
                    return
                # If we want to swap one of our unknown cards and the other card
                # is valuable enough to have a value of less than 4, swap them.
                if not card and other_card.points < 4:
                    self.controller.swap_players_cards(
                        card_num,
                        other_player_num,
                        other_card_num,
                        show_cards=show_cards,
                    )
                    return
                # The card isn't valuable enough to swap, and since we've already used our chance
                # to look at the card, choose to do nothing.
                self.controller.no_card_effect()
                return
            # If we get to this point, we don't know what the other card's value is.
            # In this case, just take a chance and swap the cards if our card has a card effect,
            # since those have a high points value.
            if card and card.has_effect():
                self.controller.swap_players_cards(
                    card_num,
                    other_player_num,
                    other_card_num,
                    show_cards=show_cards,
                )
                return
        # At this point, our worst card is either valuable enough to keep,
        # or we don't have any information about any other player's cards.
        # In any case, do nothing.
        self.controller.no_card_effect()

    def get_best_other_card_num(self) -> Tuple[int, int]:
        """
        Within the computer player's player pile memory, get the most valuable
        card owned by any of the other players.
        If we don't know any of the other players' cards, return (0, 0).
        """
        best_player_num = 0
        best_card_num = 0
        best_card: Optional[Card] = None
        for player_num, pile in self.player_piles.items():
            if player_num == self.my_player_num:
                continue
            if player_num == self.current_state.shouf_player:
                continue
            for card_num, card in pile.items():
                if not card:
                    continue
                if not best_card or card.points < best_card.points:
                    best_player_num = player_num
                    best_card_num = card_num
                    best_card = card
        return (best_player_num, best_card_num)

    def handle_card_stack(self) -> bool:
        """"""
        #
        if (
            self.current_phase == Phase.CARD_STACKED
            and self.current_state.stacked_player != self.my_player_num
        ):
            return False
        #
        try:
            #
            if self.current_state.stacked_previous_targets:
                target_player_num, target_card_num = self.current_state.stacked_previous_targets[0]
                self.controller.move_card(
                    self.get_most_replaceable_card_num(),
                    target_player_num,
                    target_card_num,
                )
                return True
            #
            stackable_card = self.get_stackable_card()
            if stackable_card:
                self.controller.stack_card(stackable_card[0], stackable_card[1])
                return True
            #
            return False
        except LegalityError as err:
            if str(err) in (
                "You cannot stack cards until after a card has been played",
                "Only the player who stacked cards first may stack additional cards",
                "Only the player who played stacked cards may move cards",
            ):
                print(f"Computer {self.my_player_num}: Missed our chance to stack")
                return False
            else:
                raise

    def get_stackable_card(self) -> Optional[Tuple[int, int]]:
        """"""
        for player_num in self.other_player_nums + [self.my_player_num]:
            if player_num == self.current_state.shouf_player:
                continue
            for card_num, card in self.player_piles[player_num].items():
                if card:
                    # If this is one of our cards and is worth negative points, skip it,
                    # because we definitely do NOT want to stack it.
                    # Having that card actually LOWERS the combined score of our cards,
                    # after all.
                    if player_num == self.my_player_num and card.points < 0:
                        continue
                    if card.rank == self.discard_card.rank:
                        return (player_num, card_num)
        return None
