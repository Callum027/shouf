# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

import random
import time

from threading import Thread, Semaphore  # Condition
from typing import TYPE_CHECKING, Dict, List, Optional, Sequence, Tuple  # List,

from shouf.core.action.listener.base import ActionListener

# from shouf.core.action.listener.cache import ActionListenerCache
from shouf.core.card import Card
from shouf.core.controller import Controller
from shouf.core.state import State, Phase

from shouf.core.exceptions import LegalityError

if TYPE_CHECKING:
    from shouf.core.engine import Engine
    from shouf.core.player.computer import ComputerPlayer


class Computer(Thread, ActionListener):
    """"""

    def __init__(self, player: ComputerPlayer, thinking_time: Optional[Tuple[float, float]]):
        """"""
        #
        self.my_player = player
        self.thinking_time = thinking_time
        #
        self.controller: Optional[Controller] = None
        self.random: Optional[random.Random] = None
        #
        self.controller_lock = Semaphore(0)  # Condition()
        self.stopped = False
        #
        self.current_turn = 0
        #
        self.hand_card: Optional[Card] = None
        self.discard_card: Optional[Card] = None
        self.player_piles: Dict[int, Dict[int, Optional[Card]]] = {}
        #
        Thread.__init__(self, name=f"Computer-{self.my_player_num}")
        ActionListener.__init__(self)

    #
    #
    #

    def subscribe_to_engine(self, engine: Engine):
        """"""
        super().subscribe_to_engine(engine)
        self.controller = Controller(self.engine, self.my_player_num)
        self.random = self.engine.random

    #
    #
    #

    @property
    def my_player_num(self) -> int:
        """"""
        return self.my_player.num

    @property
    def other_player_nums(self) -> List[int]:
        """"""
        return [pn for pn in self.player_piles.keys() if pn != self.my_player_num]

    @property
    def current_state(self) -> State:
        """"""
        return self.engine.current_state

    @property
    def current_phase(self) -> Phase:
        """"""
        return self.current_state.current_phase

    @property
    def current_player(self) -> int:
        """"""
        return self.current_state.current_player

    @property
    def player_scores(self) -> Dict[int, int]:
        """"""
        return {
            player_num: sum(card.points for card in pile.values() if card)
            for player_num, pile in self.player_piles.items()
        }

    #
    #
    #

    def run(self) -> None:
        """"""
        # self.engine.register_action_listener(self)
        while True:
            try:
                # print(f"Computer {self.my_player_num}: Waiting for lock release")
                # self.controller_lock.wait()
                self.controller_lock.acquire()
                # with self.controller_lock:
                # print(f"Computer {self.my_player_num}: Running computer loop")
                if self.stopped:
                    break
                # self.current_state = self.states.pop(0)
                if self.current_phase == Phase.END_GAME:
                    break
                # TODO: Add exception handler so that the computer doesn't die when there is a bug.
                self.handle_action()
                # print(f"Computer {self.my_player_num}: Relocking")
            except LegalityError as err:
                print(
                    f"Computer Player {self.my_player_num} "
                    f"tried to make an illegal move: {str(err)}",
                )
        # Do NOT deregister the action listener.
        # Even if this listener is alerted of an action, it won't actually do anything,
        # and removing the action listener modifies the engine state while it is being read.
        # self.engine.deregister_action_listener(self)

    def stop(self) -> None:
        """
        Send a signal to the computer thread telling it to terminate,
        and wait until it has terminated.
        """
        self.stopped = True
        self.notify_controller()
        self.join()

    def notify_controller(self) -> None:
        """
        Notify the condition variable controlling the computer player thread,
        so it can handle the next action phase.
        """
        self.controller_lock.release()
        # with self.controller_lock:
        #     self.controller_lock.notify()

    #
    #
    #

    def on_new_game(self) -> None:
        """"""
        pass

    def on_shuffle_deck(self, shuffled_deck: Sequence[Card]) -> None:
        """"""
        current_state = self.engine.current_state
        # self.add_state(current_state)
        self.hand_card = None
        self.discard_card = None
        self.player_piles = {player_num: {} for player_num in current_state.player_nums}
        # self.notify_controller()

    def on_deal_card(self, player_num: int, card_num: int) -> None:
        """
        Deal a card to the player specified in the action object.
        """
        # current_state = self.engine.current_state
        # self.add_state(current_state)
        self.player_piles[player_num][card_num] = None
        # self.notify_controller()

    def on_finish_dealing_cards(self) -> None:
        """"""
        pass

    def on_show_front_two_cards(self) -> None:
        """"""
        current_state = self.engine.current_state
        # self.add_state(current_state)
        for card_num in (2, 4):
            self.player_piles[self.my_player_num][card_num] = current_state.player_piles[
                self.my_player_num
            ][card_num]
        # self.notify_controller()

    def on_start_game(self) -> None:
        """"""
        # current_state = self.engine.current_state
        # self.add_state(current_state)
        self.current_turn = 1
        self.notify_controller()

    def on_draw_from_deck(self, player_num: int) -> None:
        """"""
        current_state = self.engine.current_state
        # self.add_state(current_state)
        if player_num == self.my_player_num:
            self.hand_card = current_state.hand_card
        self.notify_controller()

    def on_draw_from_discard(self, player_num: int) -> None:
        """"""
        current_state = self.engine.current_state
        # self.add_state(current_state)
        if current_state.discard_pile:
            self.discard_card = current_state.discard_pile.get_top_card()
        if player_num == self.my_player_num:
            self.hand_card = current_state.hand_card
        self.notify_controller()

    def on_play_card(self, player_num: int) -> None:
        """"""
        current_state = self.engine.current_state
        # self.add_state(current_state)
        self.discard_card = current_state.discard_pile.get_top_card()
        if player_num == self.my_player_num:
            self.hand_card = None
        self.notify_controller()

    def on_switch_cards(self, player_num: int, card_num: int) -> None:
        """"""
        current_state = self.engine.current_state
        # self.add_state(current_state)
        #
        self.discard_card = current_state.discard_pile.get_top_card()
        #
        if player_num == self.my_player_num:
            swapped_card = self.hand_card
            self.hand_card = None
            self.player_piles[player_num][card_num] = swapped_card
        #
        else:
            self.player_piles[player_num][card_num] = None
        #
        self.notify_controller()

    def on_swap_players_cards(
        self,
        player_num: int,
        card_num: int,
        other_player_num: int,
        other_card_num: int,
        show_cards: bool,
    ) -> None:
        """"""
        #
        current_state = self.engine.current_state
        # self.add_state(current_state)
        # If we're the one swapping cards and we're allowed to see the cards we swapped,
        # just get the card objects from the updated current state.
        if player_num == self.my_player_num and show_cards:
            self.player_piles[player_num][card_num] = current_state.player_piles[player_num][
                card_num
            ]
            self.player_piles[other_player_num][other_card_num] = current_state.player_piles[
                other_player_num
            ][other_card_num]
        # Otherwise, just swap the player card references in our memory.
        else:
            player_card = self.player_piles[player_num][card_num]
            other_card = self.player_piles[other_player_num][other_card_num]
            self.player_piles[player_num][card_num] = other_card
            self.player_piles[other_player_num][other_card_num] = player_card
        #
        self.notify_controller()

    def on_show_card(self, player_num: int, target_player_num: int, target_card_num: int) -> None:
        """"""
        current_state = self.engine.current_state
        # self.add_state(current_state)
        if player_num == self.my_player_num:
            self.player_piles[target_player_num][target_card_num] = current_state.player_piles[
                target_player_num
            ][target_card_num]
        self.notify_controller()

    def on_no_card_effect(self, player_num: int) -> None:
        """"""
        # current_state = self.engine.current_state
        # self.add_state(current_state)
        self.notify_controller()

    def on_stack_card(self, player_num: int, target_player_num: int, target_card_num: int) -> None:
        """"""
        current_state = self.engine.current_state
        # self.add_state(current_state)
        self.discard_card = current_state.discard_pile.get_top_card()
        del self.player_piles[target_player_num][target_card_num]
        self.notify_controller()

    def on_move_card(
        self,
        player_num: int,
        card_num: int,
        target_player_num: int,
        target_card_num: int,
    ) -> None:
        """"""
        # current_state = self.engine.current_state
        # self.add_state(current_state)
        player_card = self.player_piles[player_num][card_num]
        self.player_piles[target_player_num][target_card_num] = player_card
        del self.player_piles[player_num][card_num]
        self.notify_controller()

    def on_call_shouf(self, player_num: int) -> None:
        """"""
        # current_state = self.engine.current_state
        # self.add_state(current_state)
        self.notify_controller()

    def on_end_turn(self, player_num: int) -> None:
        """"""
        # current_state = self.engine.current_state
        # self.add_state(current_state)
        # print(
        #     f"Turn has ended. Computer {self.my_player_num}, "
        #     f"Current Phase: {current_state.current_phase.name}, "
        #     f"Current Player: {current_state.current_player}"
        # )
        if (
            self.current_phase == Phase.END_GAME
            or self.current_turn < self.current_state.current_turn
        ):
            self.current_turn = self.current_state.current_turn
            self.notify_controller()

    #
    #
    #

    def handle_action(self) -> None:
        """"""
        # print(
        #     f"Computer {self.my_player_num}, "
        #     f"Current Phase: {self.current_phase.name}, "
        #     f"Current Player: {self.current_player}"
        # )
        if self.current_phase in (Phase.CARD_EFFECT_USED, Phase.CARD_STACKED):
            self.think(cycles=3)
            if self.current_phase not in (Phase.CARD_EFFECT_USED, Phase.CARD_STACKED):
                return
            # print(f"Computer {self.my_player_num}: Handling card stacking")
            if not self.handle_card_stack():
                # print(f"Computer {self.my_player_num}: Ending turn")
                self.handle_end_turn()
                # print(f"Computer {self.my_player_num}: Done")
        elif self.current_phase == Phase.SHOUF_CALLED:
            # print(f"Computer {self.my_player_num}: Ending turn due to calling Shouf")
            self.think()
            if self.current_phase != Phase.SHOUF_CALLED:
                return
            self.handle_end_turn()
            # print(f"Computer {self.my_player_num}: Done")
        elif self.my_player_num == self.current_player:
            self.think()
            if self.current_phase == Phase.NEW_TURN:
                # print(f"Computer {self.my_player_num}: Drawing card")
                self.handle_draw_card()
                # print(f"Computer {self.my_player_num}: Done")
            elif self.current_phase == Phase.CARD_DRAWN:
                # print(f"Computer {self.my_player_num}: Playing card")
                self.handle_play_card()
                # print(f"Computer {self.my_player_num}: Done")
            elif self.current_phase == Phase.CARD_USED:
                # print(f"Computer {self.my_player_num}: Handling card effect")
                self.handle_card_effect()
                # print(f"Computer {self.my_player_num}: Done")

    def think(self, cycles: int = 1) -> None:
        """"""
        for cycle in range(cycles):
            time.sleep(self.random.uniform(self.thinking_time[0], self.thinking_time[1]))

    def handle_draw_card(self) -> None:
        """"""
        raise NotImplementedError()

    def handle_play_card(self) -> None:
        """"""
        raise NotImplementedError()

    def handle_card_effect(self) -> None:
        """"""
        raise NotImplementedError()

    def handle_card_stack(self) -> bool:
        """"""
        raise NotImplementedError()

    def handle_end_turn(self) -> None:
        """"""
        if self.my_player_num not in self.current_state.ended_turn_players:
            self.controller.end_turn()

    def can_play(self, for_stacking: bool = False) -> bool:
        """"""
        if for_stacking and self.current_phase == Phase.CARD_STACKED:
            return self.current_state.stacked_player == self.my_player_num
        return self.current_player == self.my_player_num
