# -*- coding: utf-8 -*-

"""
"""


from __future__ import annotations

import enum

from typing import TYPE_CHECKING

from shouf.computer.hard import HardComputer

if TYPE_CHECKING:
    from shouf.computer.base import Computer
    from shouf.core.player.computer import ComputerPlayer


class Difficulty(enum.IntEnum):
    """"""

    EASY = 0
    NORMAL = 1
    HARD = 2
    AI = 3


def create(player: ComputerPlayer) -> Computer:
    """"""

    return HardComputer(player)
