# -*- coding: utf-8 -*-

"""
Game turn.
"""


from typing import List, Optional

from shouf.core.action.base import Action
from shouf.core.action.preplay import (
    PreplayAction,
    CallShoufAction,
    DrawDeckAction,
    DrawDiscardAction,
)
from shouf.core.action.play import PlayAction, PlayCardAction  # , SwitchCardsAction
from shouf.core.action.card_effect import CardEffectAction
from shouf.core.action.postplay import PostplayAction

from shouf.core import engine as core_engine

from shouf.core.state import State


class Turn:
    """"""

    def __init__(
        self,
        engine: "core_engine.Engine",
        initial_state: State,
        initial_action: Optional[PreplayAction] = None,
        play_action: Optional[PlayAction] = None,
        card_effect_action: Optional[CardEffectAction] = None,
        post_play_actions: Optional[List[PostplayAction]] = None,
    ):
        """"""
        self.engine = engine
        self.initial_state = initial_state
        self.initial_action = initial_action
        self.play_action = play_action
        self.card_effect_action = card_effect_action
        self.post_play_actions = post_play_actions

    @property
    def current_player(self) -> int:
        """"""
        return self.initial_state.current_player

    @property
    def actions(self) -> List[Action]:
        actions: List[Action] = []
        #
        if self.initial_action:
            return actions
        actions.append(self.initial_action)
        #
        if not self.play_action:
            return actions
        actions.append(self.play_action)
        #
        if self.card_effect_action:
            actions.append(self.card_effect_action)
        #
        if not self.post_play_actions:
            return actions
        actions.extend(self.post_play_actions)
        #
        return actions

    def take(self) -> "Turn":
        """"""

        # One of three options.
        #  1. Call Shouf.
        #  2. Draw from the deck.
        #  3. Draw from the discard pile.
        self.do_initial_action()
        #
        if isinstance(self.initial_action, CallShoufAction):
            return self.get_next_turn()
        # Two possible actions.
        # If the player drew from the deck, both options are available.
        # If they drew from the discard pile, only option 1 can be done.
        #  1. Switch the picked up card with one of their face-down cards.
        #     The former face down card goes to the discard pile, face up.
        #     No card effects may be used.
        #  2. Directly play the card drew from the deck onto the discard pile.
        #     The player may choose whether or not to use the card's effect.
        self.do_play_action()
        # Wait for stacking.
        self.do_post_play_actions()
        # Return the state for the next turn.
        return self.get_next_turn()

    def do_initial_action(self) -> None:
        """"""
        self.initial_action = self.engine.get_initial_action(self.current_player)

    def do_play_action(self) -> None:
        """"""
        #
        if isinstance(self.initial_action, DrawDeckAction):
            self.play_action = self.engine.get_play_action(self.current_player, drew_from_deck=True)
            if isinstance(self.play_action, PlayCardAction):
                hand_card = self.current_state.discard_pile.get_top_card()
                if hand_card.has_effect() and self.engine.confirm_use_card_effect(
                    self.current_player,
                ):
                    self.card_effect_action = self.engine.get_card_effect_action(
                        self.current_player,
                        hand_card,
                    )
        #
        elif isinstance(self.initial_action, DrawDiscardAction):
            self.play_action = self.engine.get_play_action(
                self.current_player,
                drew_from_deck=False,
            )
        #
        else:
            pass  # raise ConsistencyError

    def do_post_play_actions(self) -> None:
        """"""
        pass

    @property
    def current_state(self) -> State:
        """"""
        cur_state = self.initial_state
        for action in self.actions:
            cur_state = action.apply(cur_state)
        return cur_state

    def get_next_turn(self) -> "Turn":
        """"""
        return Turn(engine=self.engine, initial_state=self.current_state)

    def copy(self) -> "Turn":
        """"""
        return Turn(
            engine=self.engine,
            initial_state=self.initial_state.copy(),
            initial_action=self.initial_action,
            play_action=self.play_action,
            card_effect_action=self.card_effect_action,
            post_play_actions=self.post_play_actions,
        )
